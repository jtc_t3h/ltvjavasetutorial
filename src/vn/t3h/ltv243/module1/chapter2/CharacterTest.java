package vn.t3h.ltv243.module1.chapter2;

public class CharacterTest {

	public static void main(String[] args) {

		Character ch1 = 'a'; // khai bao bien va gan gia tri
		Character ch2 = new Character('b');
		
		System.out.println(Character.isDigit(ch1));
		System.out.println(Character.isWhitespace(ch2));
	}

}
