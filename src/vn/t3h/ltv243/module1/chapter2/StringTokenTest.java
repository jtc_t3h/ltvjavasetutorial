package vn.t3h.ltv243.module1.chapter2;

import java.util.StringTokenizer;

public class StringTokenTest {

	public static void main(String[] args) {

		String hoTen = "Nguyen Van A";
		
		StringTokenizer st = new StringTokenizer(hoTen, " ");
		
		// Duyet danh sach token
		System.out.println("Count token = " + st.countTokens());
		for (int i = 0; i < st.countTokens(); i++){
			System.out.println("Token = " + st.nextToken());
		}
		
//		while (st.hasMoreTokens()) {
//			System.out.println("Token = " + st.nextToken());
//		}
//		System.out.println("Count token = " + st.countTokens()); 
		
		
	}

}
