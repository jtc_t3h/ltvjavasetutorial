package vn.t3h.ltv243.module1.chapter2;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.CardLayout;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Bai1_FrmGiaiPhuongTrinhBacNhat extends JFrame {

	private JPanel contentPane;
	private JTextField txtA;
	private JTextField txtB;
	private JTextField txtKetQua;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Bai1_FrmGiaiPhuongTrinhBacNhat frame = new Bai1_FrmGiaiPhuongTrinhBacNhat();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Bai1_FrmGiaiPhuongTrinhBacNhat() {
		setTitle("Giải phương trình bậc I");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 221);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblGiiPhngTrnh = new JLabel("Giải phương trình ax + b = 0");
		lblGiiPhngTrnh.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblGiiPhngTrnh.setHorizontalAlignment(SwingConstants.CENTER);
		lblGiiPhngTrnh.setBounds(10, 11, 414, 14);
		contentPane.add(lblGiiPhngTrnh);
		
		JLabel lblNewLabel = new JLabel("Nhập a");
		lblNewLabel.setBounds(20, 44, 86, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblNhpB = new JLabel("Nhập b");
		lblNhpB.setBounds(20, 64, 86, 14);
		contentPane.add(lblNhpB);
		
		JLabel lblKtQu = new JLabel("Kết quả");
		lblKtQu.setBounds(20, 96, 86, 14);
		contentPane.add(lblKtQu);
		
		txtA = new JTextField();
		txtA.setBounds(115, 41, 266, 20);
		contentPane.add(txtA);
		txtA.setColumns(10);
		
		txtB = new JTextField();
		txtB.setColumns(10);
		txtB.setBounds(115, 64, 266, 20);
		contentPane.add(txtB);
		
		txtKetQua = new JTextField();
		txtKetQua.setEditable(false);
		txtKetQua.setColumns(10);
		txtKetQua.setBounds(115, 93, 266, 20);
		contentPane.add(txtKetQua);
		
		JButton btnTinh = new JButton("Tính");
		btnTinh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				double a = Double.parseDouble(txtA.getText().trim());
				double b = Double.parseDouble(txtB.getText().trim());
				
				double kq = -1*b/a;
				
				txtKetQua.setText(String.valueOf(kq));
			}
		});
		btnTinh.setBounds(85, 145, 89, 23);
		contentPane.add(btnTinh);
		
		JButton btnNhapLai = new JButton("Nhập lại");
		btnNhapLai.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				txtA.setText("");
				txtB.setText("");
				txtKetQua.setText("");
			}
		});
		btnNhapLai.setBounds(220, 145, 89, 23);
		contentPane.add(btnNhapLai);
	}
}
