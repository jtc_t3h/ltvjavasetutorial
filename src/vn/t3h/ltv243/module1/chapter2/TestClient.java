package vn.t3h.ltv243.module1.chapter2;

public class TestClient {

	public final float pi = 3.14f; // instance variable 
	public static int amount = 0; // static variable
	
	public static void main(String[] args) {

		int nine = 011; // local variable (khong co modifier)
//		pi = 3.14F;
		
		System.out.println(nine);
		System.out.println("Hello");
		System.out.println(10);
		System.out.println('a');
		System.out.println(3.14);
	}

}
