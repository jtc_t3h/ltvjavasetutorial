package vn.t3h.ltv243.module1.chapter2;

public class StringTest {

	public static void main(String[] args) {

		String s1 = "Hello";
		String s2 = "Hello";
		String s3 = new String("Hello");
		
		System.out.println(s3 == s1);
		System.out.println(s1 == s2);
		System.out.println(s3.equals(s1));
		
		String s4 = "Hello";
		s4 = s4.concat(" World");
		
		System.out.println(s4); // 
		
		System.out.println(s1.compareTo(s2)); // > 0 (s1 > s2) | = 0 (s1 = s2) | < 0 (s1 < s2) => 
	}

}
