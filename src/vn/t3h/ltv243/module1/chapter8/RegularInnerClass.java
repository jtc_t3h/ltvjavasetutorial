package vn.t3h.ltv243.module1.chapter8;

public class RegularInnerClass {

	private int outerVar = 10;
	
	public class InnerClass {
		public int innerVar = 20;
		
		public void printl() {
			System.out.println("outerVar = " + outerVar);
		}
		
	}
	
	public void callInnerVar() {
		InnerClass inClass = new InnerClass();
		System.out.println(inClass.innerVar);
		
		class Local {
			private int localVar = 30;
			public void print() {
				System.out.println("localVar = " + localVar);
			}
		}
		
		Local local = new Local();
		System.out.println("local.localVar = " + local.localVar);
		local.print();
	}
}
