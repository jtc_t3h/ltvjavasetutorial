package vn.t3h.ltv243.module1.chapter8;

import vn.t3h.ltv243.module1.chapter8.RegularInnerClass;

public class TestClient {

	public static void main(String[] args) {

		RegularInnerClass regularClass = new RegularInnerClass();
		regularClass.callInnerVar();
		
		//
		RegularInnerClass.InnerClass inClass = regularClass.new InnerClass();
		System.out.println("inClass.innerVar = " + inClass.innerVar);
	}

}
