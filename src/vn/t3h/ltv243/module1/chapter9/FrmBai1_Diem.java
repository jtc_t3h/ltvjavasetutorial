package vn.t3h.ltv243.module1.chapter9;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import com.mysql.cj.core.log.Slf4JLogger;

import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.awt.event.ActionEvent;

public class FrmBai1_Diem extends JFrame {

	private JPanel contentPane;
	private JTextField txtHoTen;
	private JTextField txtDiemTBHK1;
	private JTextField txtDiemTBHK2;
	private JTable tblBangDiem;
	private JComboBox cbbKhoiLop;
	
	private String pathFile = "src/vn/t3h/ltv243/module1/chapter9/tongketnamhoc.txt";

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai1_Diem frame = new FrmBai1_Diem();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai1_Diem() {
		setTitle("Tổng kết năm học");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 499, 342);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Họ và Tên");
		lblNewLabel.setBounds(10, 11, 105, 14);
		contentPane.add(lblNewLabel);
		
		txtHoTen = new JTextField();
		txtHoTen.setBounds(126, 8, 204, 20);
		contentPane.add(txtHoTen);
		txtHoTen.setColumns(10);
		
		JButton btnThemMoi = new JButton("Thêm mới");
		btnThemMoi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// doc du lieu nguoi dung nhap vao
				String hoTen = txtHoTen.getText().trim();
				int khoiLop = Integer.parseInt(cbbKhoiLop.getSelectedItem().toString());
				double diemHK1 = Double.valueOf(txtDiemTBHK1.getText().trim());
				double diemHK2 = Double.valueOf(txtDiemTBHK2.getText().trim());
				
				Bai1_HocSinh hocSinh = new Bai1_HocSinh(hoTen, khoiLop, diemHK1, diemHK2);
				
				// ghi doi tuong vao file
				try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(pathFile, true))){
					out.writeObject(hocSinh);
					JOptionPane.showMessageDialog(null, "Ghi dữ liệu thành công.");
				} catch (IOException e) {
					JOptionPane.showMessageDialog(null, "Ghi dữ liệu lỗi.");
					e.printStackTrace();
				} 
			}
		});
		btnThemMoi.setBounds(336, 7, 137, 23);
		contentPane.add(btnThemMoi);
		
		JLabel lblKhiLp = new JLabel("Khối lớp");
		lblKhiLp.setBounds(10, 40, 105, 14);
		contentPane.add(lblKhiLp);
		
		JButton btnTiepTuc = new JButton("Tiếp tục");
		btnTiepTuc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				txtHoTen.setText("");
				cbbKhoiLop.setSelectedIndex(0);
				txtDiemTBHK1.setText("");
				txtDiemTBHK2.setText("");
			}
		});
		btnTiepTuc.setBounds(336, 36, 137, 23);
		contentPane.add(btnTiepTuc);
		
		JLabel lblimTbHk = new JLabel("Điểm TB HK1");
		lblimTbHk.setBounds(10, 69, 105, 14);
		contentPane.add(lblimTbHk);
		
		txtDiemTBHK1 = new JTextField();
		txtDiemTBHK1.setColumns(10);
		txtDiemTBHK1.setBounds(126, 66, 204, 20);
		contentPane.add(txtDiemTBHK1);
		
		JButton btnDocDS = new JButton("Đọc danh sách");
		btnDocDS.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(pathFile))){
					while (true) {
						Bai1_HocSinh hocSinh = (Bai1_HocSinh) in.readObject();
						
						// hien thi len bang ket qua
						DefaultTableModel model = (DefaultTableModel) tblBangDiem.getModel();
						model.addRow(new Object[] {hocSinh.getHoTen(), hocSinh.getKhoiLop(), hocSinh.getDiemHK1(), hocSinh.getDiemHK2(), hocSinh.getDiemTBHK()});
					}
				} catch (IOException | ClassNotFoundException e1) {
					e1.printStackTrace();
				}
			}
		});
		btnDocDS.setBounds(336, 65, 137, 23);
		contentPane.add(btnDocDS);
		
		JLabel lblimTbHk_1 = new JLabel("Điểm TB HK2");
		lblimTbHk_1.setBounds(10, 98, 105, 14);
		contentPane.add(lblimTbHk_1);
		
		txtDiemTBHK2 = new JTextField();
		txtDiemTBHK2.setColumns(10);
		txtDiemTBHK2.setBounds(126, 95, 204, 20);
		contentPane.add(txtDiemTBHK2);
		
		JButton btnThongKe = new JButton("Thống kê");
		btnThongKe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int slGioi = 0;
				int slKha = 0;
				try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(pathFile))){
					
					
					while (true) {
						Bai1_HocSinh hocSinh = (Bai1_HocSinh) in.readObject();
						
						if (hocSinh.getDiemTBHK() >= 8) {
							slGioi++;
						} else if (hocSinh.getDiemTBHK() >=7 && hocSinh.getDiemTBHK() < 8) {
							slKha++;
						}
					}
				} catch (IOException | ClassNotFoundException e1) {
					e1.printStackTrace();
				}
				
				StringBuffer message = new StringBuffer();
				message.append("Số HS giỏi: " + slGioi + "\n");
				message.append("Số HS khá: " + slKha + "\n");
				JOptionPane.showMessageDialog(contentPane, message.toString());
			}
		});
		btnThongKe.setBounds(336, 94, 137, 23);
		contentPane.add(btnThongKe);
		
		cbbKhoiLop = new JComboBox();
		cbbKhoiLop.setModel(new DefaultComboBoxModel(new String[] {"10", "11", "12"}));
		cbbKhoiLop.setBounds(126, 39, 204, 20);
		contentPane.add(cbbKhoiLop);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 123, 463, 169);
		contentPane.add(scrollPane);
		
		tblBangDiem = new JTable();
		tblBangDiem.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"H\u1ECD v\u00E0 t\u00EAn", "Kh\u1ED1i l\u1EDBp", "\u0110i\u1EC3m TB HK1", "\u0110i\u1EC3m TB HK2", "\u0110i\u1EC3m TB c\u1EA3 n\u0103m"
			}
		));
		tblBangDiem.getColumnModel().getColumn(4).setPreferredWidth(113);
		scrollPane.setViewportView(tblBangDiem);
	}
}
