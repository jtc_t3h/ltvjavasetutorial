package vn.t3h.ltv243.module1.chapter9;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class ReadFileByBufferReader {

	public static void main(String[] args) {

		try (BufferedReader br = new BufferedReader(new FileReader("src/vn/t3h/ltv243/module1/chapter9/Test.txt"))){
			String line = null;
			while ( (line = br.readLine()) != null) {
				System.out.println(line);
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
