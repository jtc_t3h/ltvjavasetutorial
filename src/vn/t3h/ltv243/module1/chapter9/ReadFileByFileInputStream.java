package vn.t3h.ltv243.module1.chapter9;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class ReadFileByFileInputStream {

	public static void main(String[] args) throws IOException {

		FileInputStream fin = null;
		try {
			fin = new FileInputStream(new File("src/vn/t3h/ltv243/module1/chapter9/Test.txt"));
			
			int c;
			while ( (c = fin.read()) != -1) {
				System.out.print((char)c);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (fin != null) fin.close();
		}
	}

}
