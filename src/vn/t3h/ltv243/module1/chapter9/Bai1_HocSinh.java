package vn.t3h.ltv243.module1.chapter9;

import java.io.Serializable;
import java.text.NumberFormat;

public class Bai1_HocSinh implements Serializable {

	// thuoc tinh (instance variable)
	private String hoTen;
	private int khoiLop;
	private double diemHK1;
	private double diemHK2;
	
	private double diemTBHK;
	
	// constructor
	public Bai1_HocSinh() {
		this.hoTen = null;
		this.khoiLop = 0;
		this.diemHK1 = 0.0;
		this.diemHK2 = 0.0;
		this.diemTBHK = 0.0;
	}
	
	
	public Bai1_HocSinh(String hoTen, int khoiLop, double diemHK1, double diemHK2) {
		
		this.hoTen = hoTen;
		this.khoiLop = khoiLop;
		this.diemHK1 = diemHK1;
		this.diemHK2 = diemHK2;
		
		this.diemTBHK = (this.diemHK1 + this.diemHK2*2)/3;
	}

	// getter va setter
	public String getHoTen() {
		return hoTen;
	}


	public void setHoTen(String hoTen) {
		this.hoTen = hoTen;
	}


	public int getKhoiLop() {
		return khoiLop;
	}


	public void setKhoiLop(int khoiLop) {
		this.khoiLop = khoiLop;
	}


	public double getDiemHK1() {
		return diemHK1;
	}


	public void setDiemHK1(double diemHK1) {
		this.diemHK1 = diemHK1;
	}


	public double getDiemHK2() {
		return diemHK2;
	}


	public void setDiemHK2(double diemHK2) {
		this.diemHK2 = diemHK2;
	}


	public double getDiemTBHK() {
		return diemTBHK;
	}


	public void setDiemTBHK(double diemTBHK) {
		this.diemTBHK = diemTBHK;
	}
	
	
	
	
}
