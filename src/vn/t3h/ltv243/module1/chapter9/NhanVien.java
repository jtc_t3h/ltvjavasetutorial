package vn.t3h.ltv243.module1.chapter9;

import java.io.Serializable;

public class NhanVien implements Serializable {

	private String ten;
	private double hsLuong;
	private double luongCB;
	private int gioiTinh;

	public NhanVien(String ten, double hsLuong, double luongCB, int gioiTinh) {
		this.ten = ten;
		this.hsLuong = hsLuong;
		this.luongCB = luongCB;
		this.gioiTinh = gioiTinh;
	}

}
