package vn.t3h.ltv243.module1.chapter9;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class ReadFileByFileInputStream2 {
	public static void main(String[] args) {
		
		// try with resource
		try (FileInputStream fin = new FileInputStream(new File("src/vn/t3h/ltv243/module1/chapter9/Test.txt"))){
			byte arrByte[] = new byte[3];
			while ( fin.read(arrByte) != -1) {
				for (byte c: arrByte) {
					System.out.print((char)c);
				}
				System.out.println();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
