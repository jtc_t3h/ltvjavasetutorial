package vn.t3h.ltv243.module1.chapter9;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class WriteFileByFileOutputStream {

	public static void main(String[] args) {

		try (FileOutputStream fout = new FileOutputStream(new File("src/vn/t3h/ltv243/module1/chapter9/Test2.txt"))){
			String s = "Write file by FileOutputStream";
			for (int idx = 0; idx < s.length(); idx++) {
				fout.write(s.charAt(idx));
			}
			fout.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
