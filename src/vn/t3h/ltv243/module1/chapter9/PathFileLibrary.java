package vn.t3h.ltv243.module1.chapter9;

import java.nio.file.Path;
import java.nio.file.Paths;

public class PathFileLibrary {

	public static void main(String[] args) {

		Path textFile = Paths.get("src/vn/t3h/ltv243/module1/chapter9/Test.txt");
		System.out.println(textFile.getFileName());
		System.out.println(textFile.getName(0));
		
	}

}
