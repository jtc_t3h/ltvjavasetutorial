package vn.t3h.ltv243.module1.chapter9;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

public class WireFileByObjectOutputStream {

	public static void main(String[] args) {

		try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("src/vn/t3h/ltv243/module1/chapter9/Test3.txt"))){
			
			NhanVien nv1 = new NhanVien("Nguyen Van A", 2.34, 5000000, 0);
			NhanVien nv2 = new NhanVien("Nguyen Van B", 2.42, 5000000, 1);
			
			out.writeObject(nv1);
			out.writeObject(nv2);
			
			out.flush();
			System.out.println("Done!");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
