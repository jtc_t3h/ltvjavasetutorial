package vn.t3h.ltv243.module1.chapter6;

public class NhanVienHanhChanh extends NhanVien{

	private double phuCap;

	public double getPhuCap() {
		return phuCap;
	}

	public void setPhuCap(double phuCap) {
		this.phuCap = phuCap;
	}
	
	@Override
	public double tinhLuong() {
		return super.tinhLuong() + phuCap;
	}
}
