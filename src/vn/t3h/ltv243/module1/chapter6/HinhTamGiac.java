package vn.t3h.ltv243.module1.chapter6;

public class HinhTamGiac extends HinhHoc{
	
	private double a, b, c;

	public HinhTamGiac() {}
	
	public HinhTamGiac(double a, double b, double c) {
		this.a = a;
		this.b = b;
		this.c = c;
	}

	@Override
	protected double tinhCV() {
		System.out.println("HinhTamGiac: tinhCV");
		return a + b + c;
	}
	
	

}
