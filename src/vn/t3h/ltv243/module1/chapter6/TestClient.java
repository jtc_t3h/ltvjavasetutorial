package vn.t3h.ltv243.module1.chapter6;

public class TestClient {

	public static void main(String[] args) {

		MyObject myObject = new MyObject();
		System.out.println(myObject.toString());
		
		// Compile: hinhhoc -> kieu du lieu la Hinh Hoc -> tinhCV()
		
		HinhHoc hinhHoc = new HinhHoc();
		hinhHoc.tinhCV(); // compile -> Hinh Hoc, runtime -> Hinh Hoc 
		
		hinhHoc = new HinhTron();
		hinhHoc.tinhCV(); // compile -> Hinh Hoc, tuntime -> Hinh Tron
		
		hinhHoc = new HinhTamGiac();
		hinhHoc.tinhCV(); // compile -> Hinh Hoc
		
		hinhHoc = new HinhChuNhat();
		hinhHoc.tinhCV(); // compile -> Hinh Hoc
				
	}

}
