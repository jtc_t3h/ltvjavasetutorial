package vn.t3h.ltv243.module1.chapter6;

public class HinhChuNhat extends HinhHoc {

	private double a, b;
	
	@Override
	protected double tinhCV() {
		System.out.println("HinhChuNhat: tinhCV");
		return (a + b)*2;
	}
}
