package vn.t3h.ltv243.module1.chapter6;

public class HinhTron extends HinhHoc {

	private double radius;
	
	public HinhTron() {
		super("abc");
		
		System.out.println("HinhTron: contructor no-arg");
	}
	
	public HinhTron(double radius) {
		this.radius = radius;
	}

	@Override
	public double tinhCV() {
		System.out.println("HinhTron: tinhCV");
		return radius*2*Math.PI;
	}
	
	
}
