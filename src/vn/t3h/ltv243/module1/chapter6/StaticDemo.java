package vn.t3h.ltv243.module1.chapter6;

public class StaticDemo {

	private String name;
	private static int counter;
	
	static {
		System.out.println("Static block");
	}

	public static void main(String[] args) {
       MyObject obj1 = new MyObject();
       MyObject obj2 = new MyObject();
       
       System.out.println(MyObject.counter);
       obj1.counter = 10;
       System.out.println(MyObject.counter);
	}

}
