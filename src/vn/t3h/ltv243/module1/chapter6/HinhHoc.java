package vn.t3h.ltv243.module1.chapter6;

public class HinhHoc {
	
	public HinhHoc() {
		System.out.println("HinhHoc: constructor no-arg");
	}
	
	public HinhHoc(String name) {
		System.out.println("HinhHoc: constructor with 1-arg");
	}


	protected double tinhCV() {
		System.out.println("HinhHoc: tinhCV");
		return 0.0;
	}
	
	public void aMethod() {
		
	}
	
	public void aMethod(String i) {}
	
	public void aMethod(int i) {}
	
	public int aMethod(String i, int a) {
		return 0;
	}
}
