package vn.t3h.ltv243.module1.chapter6;

public abstract class NhanVien {

	private final double luongCoBan = 1550000;
	private double heSo;
	
	public double getHeSo() {
		return heSo;
	}
	public void setHeSo(double heSo) {
		this.heSo = heSo;
	}
	
	public double tinhLuong() {
		return luongCoBan * heSo;
	}

}
