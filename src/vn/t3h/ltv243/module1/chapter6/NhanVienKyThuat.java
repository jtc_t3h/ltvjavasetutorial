package vn.t3h.ltv243.module1.chapter6;

public class NhanVienKyThuat extends NhanVien{

	private int soDuAn;

	public int getSoDuAn() {
		return soDuAn;
	}

	public void setSoDuAn(int soDuAn) {
		this.soDuAn = soDuAn;
	}
	
	@Override
	public double tinhLuong() {
		return super.tinhLuong() + (soDuAn*1000000);
	}
	
}
