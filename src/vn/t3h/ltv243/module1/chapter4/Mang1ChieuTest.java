package vn.t3h.ltv243.module1.chapter4;

public class Mang1ChieuTest {

	public static void main(String[] args) {

		int []iArr = new int[5];
		String []sArr = new String[5];
		
		System.out.println("Phan tu thu nhat cua mang so nguyen = " + iArr[0]); // 0
		System.out.println("Phan tu thu nhat cua mang chuoi = " + sArr[0]); // null 
		
		for (int index = 0; index <= iArr.length; index++) {
			System.out.println("Phan tu " + (index+1) + ": " + iArr[index]);
		}
	}

}
