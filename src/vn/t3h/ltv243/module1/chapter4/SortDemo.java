package vn.t3h.ltv243.module1.chapter4;

import java.util.Arrays;

public class SortDemo {

	public static void main(String[] args) {
		int arr[] = {1,4,6,5,2,9,0};
		
		Arrays.sort(arr);
		
		for (int e: arr) {
			System.out.println(e);
		}
	}

}
