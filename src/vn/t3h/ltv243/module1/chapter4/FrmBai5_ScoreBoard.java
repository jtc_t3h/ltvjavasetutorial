package vn.t3h.ltv243.module1.chapter4;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.StringTokenizer;
import java.awt.event.ActionEvent;

public class FrmBai5_ScoreBoard extends JFrame {

	private JPanel contentPane;
	private JTextField txtName;
	private JTextField txtScore;
	private JTextField txtKeyword;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai5_ScoreBoard frame = new FrmBai5_ScoreBoard();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai5_ScoreBoard() {
		setTitle("Score Board");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 509, 273);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Name");
		lblNewLabel.setBounds(10, 8, 46, 14);
		contentPane.add(lblNewLabel);
		
		txtName = new JTextField();
		txtName.setBounds(66, 5, 110, 20);
		contentPane.add(txtName);
		txtName.setColumns(10);
		
		JLabel lblScore = new JLabel("Score");
		lblScore.setBounds(205, 8, 46, 14);
		contentPane.add(lblScore);
		
		txtScore = new JTextField();
		txtScore.setColumns(10);
		txtScore.setBounds(261, 5, 110, 20);
		contentPane.add(txtScore);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 40, 361, 189);
		contentPane.add(scrollPane);
		
		JList lstMain = new JList();
		lstMain.setModel(new DefaultListModel());
		scrollPane.setViewportView(lstMain);
		
		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String name = txtName.getText().trim();
				String score = txtScore.getText().trim();
				
				DefaultListModel model = (DefaultListModel) lstMain.getModel();
				model.addElement(name + " " + score);
				
				
			}
		});
		btnAdd.setBounds(381, 4, 102, 40);
		contentPane.add(btnAdd);
		
		JButton btnSortName = new JButton("Sort Name");
		btnSortName.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DefaultListModel model = (DefaultListModel) lstMain.getModel();
				
				// Tao ra 2 mang tuong ung luu name va score
				String[] arr = new String[model.getSize()];
				
				// Thuc hien vong lap gan cac gia tri tuong ung vao 2 mang
				for (int idx = 0; idx < model.getSize(); idx++) {
					arr[idx] = (String) model.getElementAt(idx);
				}
				
				Arrays.sort(arr);
				
				// thiet lap model
				model.clear();
				for (String element: arr) {
					model.addElement(element);
				}
				
			}
		});
		btnSortName.setBounds(381, 55, 102, 40);
		contentPane.add(btnSortName);
		
		JButton btnSortScore = new JButton("Sort Score");
		btnSortScore.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DefaultListModel model = (DefaultListModel) lstMain.getModel();
				
				String[] arrTemp = new String[model.getSize()];
				for (int idx = 0; idx < model.getSize(); idx++) {
					String element = (String) model.getElementAt(idx);
					
					StringTokenizer st = new StringTokenizer(element);
					String name = st.nextToken();
					String score = st.nextToken();
					arrTemp[idx] = score + " " + name;
				}
				
				Arrays.sort(arrTemp);
				
				model.clear();
				for (String element: arrTemp) {
					StringTokenizer st = new StringTokenizer(element);
					String score = st.nextToken();
					String name = st.nextToken();
					
					model.addElement(name + " " + score);
				}
			}
		});
		btnSortScore.setBounds(381, 106, 102, 40);
		contentPane.add(btnSortScore);
		
		JButton btnFind = new JButton("Find");
		btnFind.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String keyword = txtKeyword.getText().trim();
				System.out.println(keyword);
				
				DefaultListModel model = (DefaultListModel) lstMain.getModel();
				DefaultListModel result = new DefaultListModel<>();
				for (int idx = 0; idx < model.getSize(); idx++) {
					if (((String)model.getElementAt(idx)).toLowerCase().contains(keyword.toLowerCase())){
						result.addElement(model.getElementAt(idx));
					}
				}
				
				lstMain.setModel(result);
			}
		});
		btnFind.setBounds(381, 190, 102, 40);
		contentPane.add(btnFind);
		
		txtKeyword = new JTextField();
		txtKeyword.setColumns(10);
		txtKeyword.setBounds(381, 159, 102, 20);
		contentPane.add(txtKeyword);
	}
}
