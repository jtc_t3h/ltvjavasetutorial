package vn.t3h.ltv243.module1.chapter5;

public class PhanSo {

	// Bien thuc the (instance variable)
	private int tuSo;
	private int mauSo;
	
	public PhanSo() {
	}
	
	public PhanSo(int tuSo) {
		this.tuSo = tuSo;
	}

	public PhanSo(int _tuSo, int _mauSo) {
		tuSo = _tuSo;
		mauSo = _mauSo;
	}
	
	public int getTuSo() {
		return tuSo;
	}
	
	public void setTuSo(int _tuSo) {
		tuSo = _tuSo;
	}

	public int getMauSo() {
		return mauSo;
	}

	public void setMauSo(int mauSo) {
		this.mauSo = mauSo;
	}
	
	// tinh tong 2 phan so
	public PhanSo add(PhanSo ps2) {
		int _tuSo = tuSo*ps2.getMauSo() + mauSo*ps2.getTuSo();
		int _mauSo = mauSo*ps2.getMauSo();
		
		return new PhanSo(_tuSo, _mauSo);
	}
}
