package vn.t3h.ltv243.module1.chapter5;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class FrmBai4_MuaSamOnline2 extends JFrame {

	private JPanel contentPane;
	private JTable table;
	
	public int[] arrSelect;
	public Bai4_Product[] arrProduct;

	/**
	 * Create the frame.
	 */
	public FrmBai4_MuaSamOnline2(int[] arrSelect, Bai4_Product[] arrProduct) {
		this.arrSelect = arrSelect;
		this.arrProduct = arrProduct;
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 414, 239);
		contentPane.add(scrollPane);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Product", "Price"
			}
		));
		scrollPane.setViewportView(table);
	
		for (int idx = 0; idx < arrSelect.length; idx++) {
			if (arrSelect[idx] == 1) {
				Bai4_Product selectedProduct = arrProduct[idx];
				
				DefaultTableModel model = (DefaultTableModel) table.getModel();
				model.addRow(new Object[] {selectedProduct.getName(), selectedProduct.getPrice()});
			}
		}
	}
}
