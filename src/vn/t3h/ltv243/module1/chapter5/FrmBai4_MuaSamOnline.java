package vn.t3h.ltv243.module1.chapter5;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FrmBai4_MuaSamOnline extends JFrame {

	private JPanel contentPane;
	
	private int[] arrSelect = new int[4];
	private Bai4_Product[] arrProduct = new Bai4_Product[] {
			new Bai4_Product("product 1", 70000),
			new Bai4_Product("Product 2", 100000),
			new Bai4_Product("product 3", 70000),
			new Bai4_Product("Product 4", 100000),
	};
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai4_MuaSamOnline frame = new FrmBai4_MuaSamOnline();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai4_MuaSamOnline() {
		setTitle("Click to buy");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 426, 509);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblProduct1 = new JLabel("");
		lblProduct1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (arrSelect[0] == 1) {
					arrSelect[0] = 0;
				} else {
					arrSelect[0] = 1;
				}
			}
		});
		lblProduct1.setIcon(new ImageIcon(FrmBai4_MuaSamOnline.class.getResource("/vn/t3h/resources/Whey-stand.jpg")));
		lblProduct1.setBounds(10, 11, 189, 200);
		contentPane.add(lblProduct1);
		
		JLabel lblProduct2 = new JLabel("");
		lblProduct2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (arrSelect[1] == 1) {
					arrSelect[1] = 0;
				} else {
					arrSelect[1] = 1;
				}
			}
		});
		lblProduct2.setIcon(new ImageIcon(FrmBai4_MuaSamOnline.class.getResource("/vn/t3h/resources/anabolic-whey.jpg")));
		lblProduct2.setBounds(209, 11, 189, 200);
		contentPane.add(lblProduct2);
		
		JLabel lblProduct3 = new JLabel("");
		lblProduct3.setIcon(new ImageIcon(FrmBai4_MuaSamOnline.class.getResource("/vn/t3h/resources/Hydro-Whey-1590-.jpg")));
		lblProduct3.setBounds(10, 222, 189, 200);
		contentPane.add(lblProduct3);
		
		JLabel lblProduct4 = new JLabel("");
		lblProduct4.setIcon(new ImageIcon(FrmBai4_MuaSamOnline.class.getResource("/vn/t3h/resources/bsn-syntha-6-whey.jpg")));
		lblProduct4.setBounds(209, 222, 189, 200);
		contentPane.add(lblProduct4);
		
		JButton btnNewButton = new JButton("ADD CART");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Lay danh sach san pham dang duoc chon de mua
				String sProductName = "";
				for (int idx = 0; idx < arrSelect.length; idx++) {
					if (arrSelect[idx] == 1) {
						sProductName += arrProduct[idx].getName();
					}
				}
				
				if (JOptionPane.showConfirmDialog(contentPane, "Do you want to by " + sProductName, "Confirm", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
					FrmBai4_MuaSamOnline2 frm = new FrmBai4_MuaSamOnline2(arrSelect, arrProduct);
					frm.setVisible(true);
				}
			}
		});
		btnNewButton.setBounds(10, 436, 388, 23);
		contentPane.add(btnNewButton);
	}

}
