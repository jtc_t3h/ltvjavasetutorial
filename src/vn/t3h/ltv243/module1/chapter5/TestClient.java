package vn.t3h.ltv243.module1.chapter5;

public class TestClient {

	public static void main(String[] args) {

		PhanSo ps1 = new PhanSo(1,2);
		PhanSo ps2 = new PhanSo(1,3);
		
		PhanSo result = ps1.add(ps2);
		System.out.println(result.getTuSo() + "/" + result.getMauSo());
		
		int a = 1;
		changeValue(a);
		System.out.println(a); // ? a = 1 (truyen tham tri) | 11 (truyen tham bien)
		
		int arr[] = {1};
		changeValue(arr);
		System.out.println(arr[0]); // 11
		
		varLengthArgMethod();
		varLengthArgMethod(1);
		varLengthArgMethod(1,2,3,4,5,6,7);
	}

	public static void changeValue(int a) {
		a += 10; // 11
	}
	
	public static void changeValue(int[] arr) {
		arr[0] += 10;
	}
	
	public static void varLengthArgMethod(int ... var) {
		// duyet cac doi so
		for (int e: var) {
			System.out.println(e);
		}
	}
}
