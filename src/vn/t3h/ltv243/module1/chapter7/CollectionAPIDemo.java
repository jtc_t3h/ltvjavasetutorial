package vn.t3h.ltv243.module1.chapter7;

import java.util.Arrays;
import java.util.Comparator;

public class CollectionAPIDemo implements Comparable<Integer> {

	public static void main(String[] args) {
		Integer arr[] = {1,2,3,4,5,6};
		
		Comparator<Integer> comparator = new Comparator<Integer>() {

			@Override
			public int compare(Integer o1, Integer o2) {
				return o2 - o1;
			}
		};
		Arrays.sort(arr, comparator);
		
		for(Integer element: arr) {
			System.out.println(element);
		}
	}

	@Override
	public int compareTo(Integer o) {
		// TODO Auto-generated method stub
		return 0;
	}

}
