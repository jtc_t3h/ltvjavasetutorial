package vn.t3h.ltv243.module1.chapter7;

public class GenericDemo {

	public static void main(String[] args) {

		Integer iArr[] = {1,2,3,4,5};
		String sArr[] = {"H","e","l","l","o"};
		Double dArr[] = {1.1,2.2,3.3};
		
		printArr(iArr);
		printArr(sArr);
		printArr(dArr);
	}
	
	public static <T> void printArr(T[] arr) {
		for (T element: arr) {
			System.out.println(element);
		}
	}
	
//	public static void printArr(Integer[] arr) {
//		for (Integer element: arr) {
//			System.out.println(element);
//		}
//	}
//	
//	public static void printArr(String[] arr) {
//		for (String element: arr) {
//			System.out.println(element);
//		}
//	}
//	
//	public static void printArr(Double[] arr) {
//		for (Double element: arr) {
//			System.out.println(element);
//		}
//	}

}
