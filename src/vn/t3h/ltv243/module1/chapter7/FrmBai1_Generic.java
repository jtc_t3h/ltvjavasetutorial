package vn.t3h.ltv243.module1.chapter7;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.awt.event.ActionEvent;

public class FrmBai1_Generic extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai1_Generic frame = new FrmBai1_Generic();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai1_Generic() {
		setTitle("In mang chuoi, so nguyen va so thuc");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 374, 389);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblLoaiMang = new JLabel("Loai mang");
		lblLoaiMang.setBounds(10, 11, 81, 14);
		contentPane.add(lblLoaiMang);

		JComboBox cbbType = new JComboBox();
		cbbType.setModel(new DefaultComboBoxModel(new String[] { "Chuoi", "So nguyen", "So thuc" }));
		cbbType.setBounds(97, 8, 242, 20);
		contentPane.add(cbbType);

		JLabel lblNewLabel = new JLabel("Nhap mang (cac phan tu cach nhau 1 khoang trang)");
		lblNewLabel.setBounds(20, 55, 330, 14);
		contentPane.add(lblNewLabel);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 93, 338, 78);
		contentPane.add(scrollPane);

		JTextArea taInput = new JTextArea();
		scrollPane.setViewportView(taInput);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(12, 218, 338, 78);
		contentPane.add(scrollPane_1);

		JTextArea taOutput = new JTextArea();
		scrollPane_1.setViewportView(taOutput);

		JLabel lblMangDaDuoc = new JLabel("Mang da duoc sap xep");
		lblMangDaDuoc.setBounds(10, 193, 340, 14);
		contentPane.add(lblMangDaDuoc);

		JButton btnSapXepTang = new JButton("Sap xep tang");
		btnSapXepTang.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				// Lay du lieu nguoi dung nhap
				String sInput = taInput.getText();

				// tach chuoi
				String[] arrInput = sInput.split(" ");

				// sap xep
//				Arrays.sort(arrInput);
				Object[] arrOutput = null;
				if (cbbType.getSelectedIndex() == 0) {
					arrOutput = arrInput;
				} else if (cbbType.getSelectedIndex() == 1) {
					arrOutput = new Integer[arrInput.length];
					for (int idx = 0; idx < arrInput.length; idx++) {
						arrOutput[idx] = Integer.parseInt(arrInput[idx]);
					}
					
				} else {
					arrOutput = new Double[arrInput.length];
					for (int idx = 0; idx < arrInput.length; idx++) {
						arrOutput[idx] = Double.parseDouble(arrInput[idx]);
					}
				}
				sort(arrOutput);

				// Thiet lap text cho taOutput
				StringBuilder sb = new StringBuilder("[");
				for (int idx = 0; idx < arrOutput.length; idx++) {
					if (idx == arrOutput.length - 1) {
						sb.append(arrOutput[idx]);
					} else {
						sb.append(arrOutput[idx] + ", ");
					}
				}
				sb.append("]");
				taOutput.setText(sb.toString());

			}
		});
		btnSapXepTang.setBounds(120, 316, 116, 23);
		contentPane.add(btnSapXepTang);
	}
	
	private <T> void sort(T[] arr) {
		Arrays.sort(arr);
	}
}
