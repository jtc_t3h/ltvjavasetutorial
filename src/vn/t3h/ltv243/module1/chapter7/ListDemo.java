package vn.t3h.ltv243.module1.chapter7;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

public class ListDemo {

	public static void main(String[] args) {

		List list1 = new ArrayList();
		list1.add(1);
		list1.add("abc");
		list1.add('a');
		
		// duyet cac phan tu -> for(DataType varName: collection)
		for (Object e: list1) {
			System.out.println(e);
		}
		
		// Java 8 -> su dung ham foreach va bieu thuc lambbda -> cu phap (parameter) -> { body }
		list1.forEach(par -> System.out.println(par));
		
		
		List<String> list2 = new LinkedList<>();
		list2.add("1");
		list2.add("abc");
		
		List<Integer> list3 = new Vector<>();
	}

}
