package vn.t3h.ltv243.module1.chapter7;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SetDemo {

	public static void main(String[] args) {
		Set<Double> set = new HashSet<>();
		set.add(1.1);
		set.add(2.2);
		set.add(1.1);
		
		// duyet cac phan tu
		Iterator<Double> iterator = set.iterator();
		while (iterator.hasNext()) {
			System.out.println(iterator.next());
		}
	}

}
