package vn.t3h.ltv243.module1.chapter7;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class MapDemo {

	public static void main(String[] args) {

		Map map = new HashMap<>();
		map.put(1, "Gia tri 1");
		map.put("abc", 123);
		map.put(1, "1");
		
		
		Set set = map.entrySet();
		Iterator iterator = set.iterator();
		while(iterator.hasNext()) {
			Entry entry = (Entry) iterator.next();
			System.out.println(entry.getKey() + " = " + entry.getValue());
		}
	}

}
