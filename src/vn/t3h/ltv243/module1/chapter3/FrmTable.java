package vn.t3h.ltv243.module1.chapter3;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FrmTable extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JButton btnAppendRow;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmTable frame = new FrmTable();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmTable() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 414, 156);
		contentPane.add(scrollPane);
		
		table = new JTable();	
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int rowIndex = table.getSelectedRow();
				JOptionPane.showMessageDialog(null, "Ban dang chon dong co Id = " + table.getValueAt(rowIndex, 0) + " va Name = " + table.getValueAt(rowIndex, 1));
			}
		});
		
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"ID", "Name"
			}
		) {
			Class[] columnTypes = new Class[] {
				Integer.class, String.class
			};
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});
		
		// load Model cho table
		// B1: Tao ra 2 mang: mang 1 chieu (header), mang 2 chieu (data)
		String [] header = {"ID", "Name"};
		Object [] data [] = {{1, "Nguyen Van A"},{2, "Nguyen Van B"}};
		
		// B2: Tao ra doi tuong DefaultTableModel
		DefaultTableModel model = new DefaultTableModel(data, header);
		
		// B3: Thiet gia tri cho Model
		table.setModel(model);
		
		scrollPane.setViewportView(table);
		
		JButton btnNewButton = new JButton("Load new data");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DefaultTableModel model = (DefaultTableModel) table.getModel();
				
				// clear old data
				while (model.getRowCount() != 0) {
					model.removeRow(model.getRowCount() - 1);
				}
			}
		});
		btnNewButton.setBounds(35, 196, 120, 23);
		contentPane.add(btnNewButton);
		
		btnAppendRow = new JButton("Append row");
		btnAppendRow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DefaultTableModel model = (DefaultTableModel) table.getModel();
				
				model.addRow(new Object [] {3, "Nguyen Van C"});
				
				
			}
		});
		btnAppendRow.setBounds(216, 196, 131, 23);
		contentPane.add(btnAppendRow);
	}
}
