package vn.t3h.ltv243.module1.chapter3;

public class Student {

	private int id;
	private String name;

	public Student(int id, String name) {
		this.id = id;
		this.name = name;
	}
	
	@Override
	public String toString(){
		return name;
	}

}
