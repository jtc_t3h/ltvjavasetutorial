package vn.t3h.ltv243.module1.chapter3;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JCheckBox;
import javax.swing.JRadioButton;
import javax.swing.border.TitledBorder;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class DialogDemo extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private ButtonGroup genderGroup = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			DialogDemo dialog = new DialogDemo();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public DialogDemo() {
		setTitle("Dialog Demo1!");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JCheckBox chckbxNewCheckBox = new JCheckBox("New check box");
		chckbxNewCheckBox.setBounds(21, 20, 97, 23);
		contentPanel.add(chckbxNewCheckBox);
		
		JCheckBox chckbxNewCheckBox_1 = new JCheckBox("New check box");
		chckbxNewCheckBox_1.setBounds(21, 46, 97, 23);
		contentPanel.add(chckbxNewCheckBox_1);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Gioi tinh", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(282, 11, 142, 122);
		contentPanel.add(panel);
		panel.setLayout(null);
		
		JRadioButton rdbtnNu = new JRadioButton("Nu");
		rdbtnNu.setBounds(23, 59, 91, 23);
		panel.add(rdbtnNu);
		genderGroup.add(rdbtnNu);
		
		JRadioButton rdbtnNam = new JRadioButton("Nam");
		rdbtnNam.setBounds(23, 30, 91, 23);
		panel.add(rdbtnNam);
		rdbtnNam.setSelected(true);
		genderGroup.add(rdbtnNam);
		
		JComboBox comboBox = new JComboBox();
		comboBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				Student student = (Student) comboBox.getSelectedItem();
				JOptionPane.showMessageDialog(rootPane, "Ban dang chon item " + student.toString());
			}
		});
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Item 1", "Item 2", "Item 3"}));
		comboBox.setBounds(24, 109, 248, 20);
		contentPanel.add(comboBox);
		
		// Cach load du lieu cho Combobox
		// 1. Tao ra mang du lieu (co kieu bat ki)
		Student[] arr = new Student []{new Student(1,"Nguyen Van A"), new Student(2, "Nguyen Van B")};
		// 2. Tao doi tuong DefaultComboboxModel
		DefaultComboBoxModel model = new DefaultComboBoxModel<>(arr);
		// 3. thiet lap thuoc tinh model cho Combobox
		comboBox.setModel(model);
	}
}
