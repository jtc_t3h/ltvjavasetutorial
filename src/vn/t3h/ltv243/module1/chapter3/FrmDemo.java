package vn.t3h.ltv243.module1.chapter3;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import java.awt.Font;
import javax.swing.JTextField;

public class FrmDemo extends JFrame {

	private JPanel contentPane;
	private JTextField txtName;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmDemo frame = new FrmDemo();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmDemo() {
		setTitle("Frame demo!");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JButton btnOpenDialog = new JButton("Open Dialog");
		btnOpenDialog.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DialogDemo dlg = new DialogDemo();
				dlg.setModal(true);
				dlg.setVisible(true);
			}
		});
		contentPane.add(btnOpenDialog, BorderLayout.CENTER);
		
		JLabel lblLeft = new JLabel("Left");
		contentPane.add(lblLeft, BorderLayout.WEST);
		
		JLabel lblTop = new JLabel("top");
		lblTop.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblTop.setToolTipText("Top tooltiptext");
		lblTop.setIcon(new ImageIcon("C:\\Users\\MoonLuna\\Downloads\\images.png"));
		lblTop.setVerticalTextPosition(JLabel.CENTER);
		lblTop.setHorizontalTextPosition(JLabel.CENTER);
		contentPane.add(lblTop, BorderLayout.NORTH);
		
		txtName = new JTextField();
		contentPane.add(txtName, BorderLayout.SOUTH);
		txtName.setColumns(10);
		
		JButton btnBrowser = new JButton("Browser");
		btnBrowser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser jfc = new JFileChooser();
				
				int returnValue = jfc.showOpenDialog(null);
				if (returnValue == JFileChooser.APPROVE_OPTION){
					File selected = jfc.getSelectedFile();
					JOptionPane.showMessageDialog(null, "Ban dang chon file " + selected.getAbsolutePath());
					txtName.setText(selected.getAbsolutePath());
				}
			}
		});
		contentPane.add(btnBrowser, BorderLayout.EAST);
		setResizable(false);
	}

}
