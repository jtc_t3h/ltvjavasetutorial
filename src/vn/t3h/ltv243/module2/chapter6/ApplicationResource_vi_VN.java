package vn.t3h.ltv243.module2.chapter6;

import java.util.ListResourceBundle;

public class ApplicationResource_vi_VN extends ListResourceBundle{

	@Override
	protected Object[][] getContents() {
		return new Object[][] {
			{"username", "Tai Khoan"}, 
			{"password", "Mat Khau"},
			{"label.select.language", "Chon ngon ngu"},
			{"combobox.select.default", "--- Chon ---"},
			{"combobox.select.vi", "Viet Nam"},
			{"combobox.select.en", "Tieng Anh"}
		};
	}

}
