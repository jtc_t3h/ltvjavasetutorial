package vn.t3h.ltv243.module2.chapter6;

import java.util.Locale;
import java.util.ResourceBundle;

public class TestClient {

	public static void main(String[] args) {
		Locale locale = new Locale("vi", "VN");
		ResourceBundle bundle = ResourceBundle.getBundle("vn.t3h.ltv243.module2.chapter6.test", locale);
		
		System.out.println(bundle.getString("username"));
		System.out.println(bundle.getString("password"));
	}

}
