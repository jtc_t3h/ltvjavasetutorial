package vn.t3h.ltv243.module2.chapter6;

public enum MyEnum {
  
	mon(1, "abc"), tue(2, "def");
	
	private int id;
	private String value;
	
	private MyEnum(int id, String value) {
		this.id = id;
		this.value = value;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	
}
