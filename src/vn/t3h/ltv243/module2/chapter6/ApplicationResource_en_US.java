package vn.t3h.ltv243.module2.chapter6;

import java.util.ListResourceBundle;

public class ApplicationResource_en_US extends ListResourceBundle {

	@Override
	protected Object[][] getContents() {
		return new Object[][] { 
			{ "username", "UserName" }, 
			{ "password", "Password" },
			{"label.select.language", "Select language"},
			{"combobox.select.default", "-- select --"},
			{"combobox.select.vi", "vietnamese"},
			{"combobox.select.en", "english"}
		};
	}
}
