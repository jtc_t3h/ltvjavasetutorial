package vn.t3h.ltv243.module2.chapter6;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FrmMultiLanguage extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JPasswordField passwordField;
	private JLabel lblSelect;
	private JLabel lblUserName;
	private JLabel lblPassword;
	private JComboBox cbbSelect;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmMultiLanguage frame = new FrmMultiLanguage();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmMultiLanguage() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 198);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblSelect = new JLabel("New label");
		lblSelect.setBounds(10, 11, 111, 14);
		contentPane.add(lblSelect);
		
		cbbSelect = new JComboBox();
		cbbSelect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		cbbSelect.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (cbbSelect.getSelectedIndex() == 1) {
					initLanguage("vi", "VN");
				} else if (cbbSelect.getSelectedIndex() == 2) {
					initLanguage("en", "US");
				}
			}
		});
		cbbSelect.setBounds(143, 8, 281, 20);
		contentPane.add(cbbSelect);
		
		lblUserName = new JLabel("New label");
		lblUserName.setBounds(10, 77, 111, 14);
		contentPane.add(lblUserName);
		
		textField = new JTextField();
		textField.setBounds(143, 74, 281, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		lblPassword = new JLabel("New label");
		lblPassword.setBounds(10, 114, 127, 14);
		contentPane.add(lblPassword);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(143, 105, 281, 20);
		contentPane.add(passwordField);
		
		initLanguage("vi", "VN");
		
	}

	private void initLanguage(String languageCode, String countryCode) {
		Locale locale = new Locale(languageCode, countryCode);
		ResourceBundle bundle = ResourceBundle.getBundle("vn.t3h.ltv243.module2.chapter6.ApplicationResource", locale);
		
		lblSelect.setText(bundle.getString("label.select.language"));
		lblUserName.setText(bundle.getString("username"));
		lblPassword.setText(bundle.getString("password"));
		
		cbbSelect.removeAll();
		cbbSelect.addItem(bundle.getString("combobox.select.default"));
		cbbSelect.addItem(bundle.getString("combobox.select.vi"));
		cbbSelect.addItem(bundle.getString("combobox.select.en"));
	}
}
