package vn.t3h.ltv243.module2.chapter6;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.awt.event.ActionEvent;

public class FrmBai2_PathFileValidator extends JFrame {

	private JPanel contentPane;
	private JTextField txtPattern;
	private JTextField txtImage;
	private JTextField txtKQ;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai2_PathFileValidator frame = new FrmBai2_PathFileValidator();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai2_PathFileValidator() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 539, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblMauDinhDang = new JLabel("Mau dinh dang Image file");
		lblMauDinhDang.setBounds(10, 11, 144, 14);
		contentPane.add(lblMauDinhDang);
		
		txtPattern = new JTextField();
		txtPattern.setEditable(false);
		txtPattern.setText("[A-Za-z0-9.]+\\.(jpg|png|gif|PNG|jPg)");
		txtPattern.setBounds(167, 8, 346, 20);
		contentPane.add(txtPattern);
		txtPattern.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Image file kiem tra");
		lblNewLabel.setBounds(10, 50, 144, 14);
		contentPane.add(lblNewLabel);
		
		txtImage = new JTextField();
		txtImage.setBounds(167, 47, 346, 20);
		contentPane.add(txtImage);
		txtImage.setColumns(10);
		
		JButton btnKiemTra = new JButton("Kiem tra");
		btnKiemTra.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String sPattern = txtPattern.getText();
				Pattern pattern = Pattern.compile(sPattern);
				
				String imageFile = txtImage.getText();
				Matcher matcher = pattern.matcher(imageFile);
				if (matcher.matches()) {
					txtKQ.setText("Image hop le");
				} else {
					txtKQ.setText("Image khong hop le");
				}
				
			}
		});
		btnKiemTra.setBounds(168, 103, 89, 23);
		contentPane.add(btnKiemTra);
		
		JLabel lblKetQua = new JLabel("Ket qua");
		lblKetQua.setBounds(10, 163, 144, 14);
		contentPane.add(lblKetQua);
		
		txtKQ = new JTextField();
		txtKQ.setEditable(false);
		txtKQ.setBounds(171, 160, 342, 20);
		contentPane.add(txtKQ);
		txtKQ.setColumns(10);
	}
}
