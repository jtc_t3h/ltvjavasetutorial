package vn.t3h.ltv243.module2.chapter9.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import vn.t3h.ltv243.module2.chapter9.entity.ThuongHieu;

public class ThuongHieuDAOImpl extends QLCSDL implements ThuongHieuDAO{

	@Override
	public List<ThuongHieu> findAll() {
		List<ThuongHieu> listOfThuongHieu = new ArrayList<>();
		try {
			openConnection();
			
			// Thuc hien query va xu ly ket qua tra ve (danh sach doi tuong)
			statement = connection.createStatement();
			resultSet = statement.executeQuery("select * from thuonghieu");
			while (resultSet.next()) {
				ThuongHieu thuongHieu = new ThuongHieu();
				thuongHieu.setId(resultSet.getInt(0));
				thuongHieu.setTenThuongHieu(resultSet.getString("tenthuonghieu"));
				thuongHieu.setHinhAnh(resultSet.getString("hinhanh"));
				
				listOfThuongHieu.add(thuongHieu);
			}
			
			closeConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listOfThuongHieu;
	}

	@Override
	public ThuongHieu findByid(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ThuongHieu> findByProperties(Map<String, Object> properties) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int insert(ThuongHieu thuongHieu) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int update(ThuongHieu thuongHieu) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int delete(int[] ids) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int delete(ThuongHieu thuongHieu) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int delele(ThuongHieu[] thuongHieus) {
		// TODO Auto-generated method stub
		return 0;
	}

}
