package vn.t3h.ltv243.module2.chapter9.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import vn.t3h.ltv243.module2.chapter9.entity.Loai;

public class LoaiDAOImpl extends QLCSDL implements LoaiDAO {

	public List<Loai> findAll() {
		List<Loai> listOfLoai = new ArrayList<>();
		try {
			openConnection();
			
			// Thuc hien query va xu ly ket qua tra ve (danh sach doi tuong)
			statement = connection.createStatement();
			resultSet = statement.executeQuery("select * from loai");
			while (resultSet.next()) {
				Loai loai = new Loai();
				loai.setId(resultSet.getInt(0));
				loai.setTenLoai(resultSet.getString("tenloai"));
				
				listOfLoai.add(loai);
			}
			
			closeConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listOfLoai;
	}

	public Loai findByid(int id) {
		Loai loai = null;
		try {
			openConnection();
			
			// Thuc hien query va xu ly ket qua query
			prepareStatement = connection.prepareStatement("select * from loai where id = ?");
			prepareStatement.setInt(1, id);
			
			resultSet = prepareStatement.executeQuery();
			while(resultSet.next()) {
				loai = new Loai();
				loai.setId(resultSet.getInt("id"));
				loai.setTenLoai(resultSet.getString("tenloai"));
			}
			
			closeConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return loai;
	}

	public List<Loai> findByProperties(Map<String, Object> properties) {
		try {
			openConnection();
			
			
			
			closeConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public int insert(Loai loai) {
		int count = 0;
		try {
			openConnection();
			
			prepareStatement = connection.prepareStatement("insert into loai(tenloai) values (?)");
			prepareStatement.setString(1, loai.getTenLoai());
			
			count = prepareStatement.executeUpdate();
			
			closeConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return count;
	}

	public int update(Loai loai) {

		return 0;
	}

	public int delete(int id) {
		int count = 0;
		try {
			openConnection();
			
			prepareStatement = connection.prepareStatement("delete from loai where id = ?");
			prepareStatement.setInt(1, id);
			
			count = prepareStatement.executeUpdate();
			
			closeConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return count;
	}

	public int delete(int[] ids) {
		int count = 0;
		
		for (int id: ids) {
			count += delete(id);
		}
		
		return count;
	}

	public int delete(Loai loai) {
		int count = 0;
		try {
			openConnection();
			
			prepareStatement = connection.prepareStatement("delete from loai where id = ?");
			prepareStatement.setInt(1, loai.getId());
			
			count = prepareStatement.executeUpdate();
			
			closeConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return count;
	}

	public int delele(Loai[] loais) {
		return 0;
	}
}
