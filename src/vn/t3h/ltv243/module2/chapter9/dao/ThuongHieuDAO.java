package vn.t3h.ltv243.module2.chapter9.dao;

import java.util.List;
import java.util.Map;

import vn.t3h.ltv243.module2.chapter9.entity.ThuongHieu;

public interface ThuongHieuDAO {
	
	public List<ThuongHieu> findAll();

	public ThuongHieu findByid(int id);

	public List<ThuongHieu> findByProperties(Map<String, Object> properties);

	public int insert(ThuongHieu thuongHieu);

	public int update(ThuongHieu thuongHieu);

	public int delete(int id);

	public int delete(int[] ids);

	public int delete(ThuongHieu thuongHieu);

	public int delele(ThuongHieu[] thuongHieus);
}
