package vn.t3h.ltv243.module2.chapter9.dao;

import java.util.List;
import java.util.Map;

import vn.t3h.ltv243.module2.chapter9.entity.Loai;

public interface LoaiDAO {

	public List<Loai> findAll();

	public Loai findByid(int id);

	public List<Loai> findByProperties(Map<String, Object> properties);

	public int insert(Loai loai);

	public int update(Loai loai);

	public int delete(int id);

	public int delete(int[] ids);

	public int delete(Loai loai);

	public int delele(Loai[] loais);
}
