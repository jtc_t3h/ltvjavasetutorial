package vn.t3h.ltv243.module2.chapter9.dao;

import vn.t3h.ltv243.module2.chapter9.entity.NguoiDung;

public class NguoiDungDAOImpl extends QLCSDL implements NguoiDungDAO {

	@Override
	public NguoiDung findByEmail(String email) {
		NguoiDung nguoiDung = null;
		try {
			openConnection();
			
			prepareStatement = connection.prepareStatement("select * from nguoidung where email = ?");
			prepareStatement.setString(1, email);
			
			resultSet = prepareStatement.executeQuery();
			while (resultSet.next()) {
				nguoiDung = new NguoiDung();
				
				nguoiDung.setId(resultSet.getInt("id"));
				nguoiDung.setEmail(resultSet.getString("email"));
				nguoiDung.setPassword(resultSet.getString("password"));
				nguoiDung.setHoTen(resultSet.getString("hoten"));
			}
			
			closeConnection();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return nguoiDung;
	}

}
