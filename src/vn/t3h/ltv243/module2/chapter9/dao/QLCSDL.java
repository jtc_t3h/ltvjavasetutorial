package vn.t3h.ltv243.module2.chapter9.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public abstract class QLCSDL {

	protected Connection connection;
	protected Statement statement;
	protected PreparedStatement prepareStatement;
	protected ResultSet resultSet;
	
	protected void openConnection() {
		String url = "jdbc:mysql://localhost:3306/phuong_perfume";
		String username = "root";
		String password = "";
		
		try {
			connection = DriverManager.getConnection(url, username, password);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	protected void closeConnection() throws SQLException {
		if (resultSet != null) {
			resultSet.close();
		}
		
		if (statement != null) {
			statement.close();
		}
		
		if (prepareStatement != null) {
			prepareStatement.close();
		}
		
		if (connection != null) {
			connection.close();
		}
	}
}
