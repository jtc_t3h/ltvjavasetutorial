package vn.t3h.ltv243.module2.chapter9.dao;

import vn.t3h.ltv243.module2.chapter9.entity.NguoiDung;

public interface NguoiDungDAO {

	public NguoiDung findByEmail(String email);
}
