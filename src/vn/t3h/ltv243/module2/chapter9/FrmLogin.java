package vn.t3h.ltv243.module2.chapter9;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import vn.t3h.ltv243.module2.chapter9.dao.NguoiDungDAO;
import vn.t3h.ltv243.module2.chapter9.dao.NguoiDungDAOImpl;
import vn.t3h.ltv243.module2.chapter9.entity.NguoiDung;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FrmLogin extends JFrame {

	private NguoiDungDAO nguoiDungDAO = new NguoiDungDAOImpl();
	
	private JPanel contentPane;
	private JTextField txtEmail;
	private JPasswordField txtPassword;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmLogin frame = new FrmLogin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmLogin() {
		setTitle("Đăng nhập");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 286);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblEmail = new JLabel("Email");
		lblEmail.setBounds(29, 33, 63, 14);
		contentPane.add(lblEmail);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(29, 63, 63, 14);
		contentPane.add(lblPassword);
		
		txtEmail = new JTextField();
		txtEmail.setBounds(102, 30, 285, 20);
		contentPane.add(txtEmail);
		txtEmail.setColumns(10);
		
		txtPassword = new JPasswordField();
		txtPassword.setBounds(102, 60, 285, 20);
		contentPane.add(txtPassword);
		
		JButton btnDangNhap = new JButton("Đăng nhập");
		btnDangNhap.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String email = txtEmail.getText().trim();
				String password = String.valueOf(txtPassword.getPassword());
				
				NguoiDung nguoiDung = nguoiDungDAO.findByEmail(email);
				if (nguoiDung.getPassword().equals(password)) {
					dispose();
					
					FrmMain frmMain = new FrmMain();
					frmMain.setWelcome(nguoiDung.getHoTen());
					frmMain.setVisible(true);
				} else {
					JOptionPane.showMessageDialog(null, "Email/ password không đúng. Xin mời nhập lại.");
				}
				
			}
		});
		btnDangNhap.setBounds(260, 101, 127, 23);
		contentPane.add(btnDangNhap);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(FrmLogin.class.getResource("/vn/t3h/ltv243/module2/chapter9/image/login-background.jpg")));
		lblNewLabel.setBounds(0, 0, 434, 247);
		contentPane.add(lblNewLabel);
	}
}
