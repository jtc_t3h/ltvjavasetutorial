package vn.t3h.ltv243.module2.chapter9;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import com.jgoodies.forms.factories.DefaultComponentFactory;

public class FrmMain extends JFrame {

	private JPanel contentPane;
	private JLabel lblWelcome;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmMain frame = new FrmMain();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmMain() {
		setTitle("Quản lý Perfume Shop");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 846, 488);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 830, 21);
		contentPane.add(menuBar);
		
		JMenu mnQunLSn = new JMenu("Quản lý sản phẩm");
		menuBar.add(mnQunLSn);
		
		JMenuItem mntmQunLThng = new JMenuItem("Quản lý thương hiệu");
		mnQunLSn.add(mntmQunLThng);
		
		JMenuItem mntmQunLLoi = new JMenuItem("Quản lý loại");
		mnQunLSn.add(mntmQunLLoi);
		
		JMenu mnQunLSn_1 = new JMenu("Quản lý sản phầm");
		mnQunLSn.add(mnQunLSn_1);
		
		JMenuItem mntmThmSpMi = new JMenuItem("Thêm SP mới");
		mntmThmSpMi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FrmThemSPMoi frm = new FrmThemSPMoi();
				frm.setVisible(true);
			}
		});
		mnQunLSn_1.add(mntmThmSpMi);
		
		JMenuItem mntmCpNhtThng = new JMenuItem("Cập nhật thông tin SP");
		mnQunLSn_1.add(mntmCpNhtThng);
		
		JMenu mnQunLn = new JMenu("Quản lý đơn hàng");
		menuBar.add(mnQunLn);
		
		JMenu mnQunLQung = new JMenu("Quản lý quảng cáo");
		menuBar.add(mnQunLQung);
		
		JMenu mnQunLNgi = new JMenu("Quản lý người dùng");
		menuBar.add(mnQunLNgi);
		
		lblWelcome = new JLabel("New label");
		lblWelcome.setHorizontalAlignment(SwingConstants.RIGHT);
		lblWelcome.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 13));
		lblWelcome.setForeground(Color.RED);
		lblWelcome.setBounds(10, 32, 703, 30);
		contentPane.add(lblWelcome);
		
		JButton btnLogout = new JButton("Logout");
		btnLogout.setBounds(723, 32, 97, 30);
		contentPane.add(btnLogout);
	}
	
	public void setWelcome(String hoTen) {
		this.lblWelcome.setText("Chào mừng bạn " + hoTen);
	}
}
