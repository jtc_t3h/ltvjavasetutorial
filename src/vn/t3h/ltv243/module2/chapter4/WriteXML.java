package vn.t3h.ltv243.module2.chapter4;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class WriteXML {

	public static void main(String[] args) throws ParserConfigurationException, TransformerException {
		DocumentBuilderFactory factory =  DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.newDocument();
		
		// tao SinhViens node -> root node
		Element sinhViensNode = document.createElement("SinhViens");
		document.appendChild(sinhViensNode);
		
		// SinhVien Node
		Element sinhVienNode = document.createElement("SinhVien");
		sinhViensNode.appendChild(sinhVienNode);
		
		// hoten node
		Element hotenNode = document.createElement("hoten");
		hotenNode.setTextContent("Nguyen Van An");
		sinhVienNode.appendChild(hotenNode);
		
		//
		File xmlFile = new File("src/vn/t3h/ltv243/module2/chapter4/demo2.xml");
		TransformerFactory transFactory = TransformerFactory.newInstance();
		Transformer tranformer = transFactory.newTransformer();
		
		DOMSource source = new DOMSource(document);
		StreamResult result = new StreamResult(xmlFile);
		tranformer.transform(source, result);
		System.out.println("Done!");
	}

}
