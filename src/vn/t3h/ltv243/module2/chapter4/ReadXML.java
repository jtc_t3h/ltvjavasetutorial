package vn.t3h.ltv243.module2.chapter4;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ReadXML {

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory factory =  DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.parse(new File("src/vn/t3h/ltv243/module2/chapter4/demo.xml"));
		
		// root node = bookstore node
		Element bookStoreNode = document.getDocumentElement();
		System.out.println(bookStoreNode.getNodeName());
		
		NodeList bookstoreChildsNode = bookStoreNode.getChildNodes();
		System.out.println(bookstoreChildsNode.getLength());
		for (int idx = 0; idx < bookstoreChildsNode.getLength(); idx++) {
			Node bookstorechildNode = bookstoreChildsNode.item(idx);
			if ("book".equals(bookstorechildNode.getNodeName())) {
				
			}
		}
		
		// duyet book node -> 2 book node
		NodeList bookNodeList = document.getElementsByTagName("book");
		System.out.println(bookNodeList.getLength());
		for(int idx = 0; idx < bookNodeList.getLength(); idx++) {
			Node bookNode = bookNodeList.item(idx);
			
			// attribute node (of book node)
			NamedNodeMap bookAttributesNode = bookNode.getAttributes();
			Node categoryNode = bookAttributesNode.item(0);
			System.out.println(categoryNode.getNodeName() + "=" + categoryNode.getNodeValue());
		}
		
		// duyet title node
		NodeList titleNodeList = document.getElementsByTagName("title");
		for (int idx = 0; idx < titleNodeList.getLength(); idx++) {
			Node titleNode = titleNodeList.item(idx);
			
			// lay body content = body text
			System.out.println(titleNode.getTextContent());
		}
	}

}
