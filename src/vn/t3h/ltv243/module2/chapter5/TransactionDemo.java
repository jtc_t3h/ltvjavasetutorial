package vn.t3h.ltv243.module2.chapter5;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;


public class TransactionDemo {

	public static void main(String[] args) throws SQLException {
		
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = JDBCUtil.getConnection();
//			con.setAutoCommit(false);
			ps = con.prepareStatement("insert into don_vi values (?,?)");
			
			ps.setInt(1, 7);
			ps.setString(2, "Don vi G");
			ps.executeUpdate();
			

			ps.setInt(1, 6);
			ps.setString(2, "Don vi F");
			ps.executeUpdate();
			
			System.out.println("Done!");
//			con.commit();
		} catch (SQLException e) {
//			if (con != null) con.rollback();
			e.printStackTrace();
		} finally {
			if (con != null) con.setAutoCommit(true);
			JDBCUtil.close(null, ps, con);
		}
	}
}
