package vn.t3h.ltv243.module2.chapter5;

public class ConnectionPoolingTest {

	public static void main(String[] args) {

//		ObjectPooling obp1 = new ObjectPooling();
//		obp1.println();
		
//		ObjectPooling obp2 = new ObjectPooling();
		
		ObjectConnectionPooling ocp = new ObjectConnectionPooling();
		ObjectPooling obp1 = ocp.checkOut();
		obp1.println();
		
		ocp.checkIn(obp1);
		
		ObjectPooling obp2 = ocp.checkOut();
		obp2.println();
		
	}

}
