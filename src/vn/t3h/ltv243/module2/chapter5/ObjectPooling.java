package vn.t3h.ltv243.module2.chapter5;

public class ObjectPooling {

	public ObjectPooling()  {
		try {
			Thread.sleep(5000);
			System.out.println("create instance: objectPooling.");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void println() {
		System.out.println("do something.");
	}
}
