package vn.t3h.ltv243.module2.chapter5;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JDBCTest2 {

	public static void main(String[] args) {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (Exception e) {
			System.out.println("missing driver.");
		}
		
		String url = "jdbc:mysql://localhost:3306/phan_cong_nhan_vien_1_1_nam";
		String username = "root";
		String password = "";
		
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		
		try {
			connection = DriverManager.getConnection(url, username, password);
			
			DatabaseMetaData metaData = connection.getMetaData();
			System.out.println(metaData.getDatabaseProductName());
			System.out.println(metaData.getDatabaseProductVersion());
			
			statement = connection.createStatement();
			resultSet = statement.executeQuery("select * from nhan_vienssss");
			
			while(resultSet.next()) {
				System.out.print("ID = " + resultSet.getInt(1));
				System.out.print("\t");
				System.out.println("Ho_ten = " + resultSet.getString("Ho_ten") );
			}
			
			System.out.println("Done.");
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("Connection fail.");
		} finally {
			try {
				statement.close();
				resultSet.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		}

	}

}
