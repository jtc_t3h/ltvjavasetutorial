package vn.t3h.ltv243.module2.chapter5;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JDBCTest3 {

	public static void main(String[] args) {
		String url = "jdbc:mysql://localhost:3306/phan_cong_nhan_vien_1_1_nam";
		String username = "root";
		String password = "";

		try (Connection connection = DriverManager.getConnection(url, username, password);
				Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery("select * from nhan_vienssss")) {
			DatabaseMetaData metaData = connection.getMetaData();
			System.out.println(metaData.getDatabaseProductName());
			System.out.println(metaData.getDatabaseProductVersion());

			while (resultSet.next()) {
				System.out.print("ID = " + resultSet.getInt(1));
				System.out.print("\t");
				System.out.println("Ho_ten = " + resultSet.getString("Ho_ten"));
			}

			System.out.println("Done.");
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("Connection fail.");
		}
	}

}
