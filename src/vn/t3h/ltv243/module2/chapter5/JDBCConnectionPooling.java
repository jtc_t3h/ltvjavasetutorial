package vn.t3h.ltv243.module2.chapter5;

import java.sql.Connection;
import java.sql.SQLException;

public class JDBCConnectionPooling extends ConnectionPooling<Connection>{

	@Override
	public Connection create() {
		try {
			return JDBCUtil.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void delete(Connection t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void exipire() {
		// TODO Auto-generated method stub
		
	}

}
