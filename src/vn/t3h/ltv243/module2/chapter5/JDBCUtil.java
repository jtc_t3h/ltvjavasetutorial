package vn.t3h.ltv243.module2.chapter5;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JDBCUtil {

	public static Connection getConnection() throws SQLException {
		String url = "jdbc:mysql://localhost:3306/phan_cong_nhan_vien_1_1_nam";
		String username = "root";
		String password = "";
		
		return DriverManager.getConnection(url, username, password);
	}
	
	public static void close(ResultSet rs, Statement st, Connection con) throws SQLException {
		if (rs != null) rs.close();
		if (st != null) st.close();
		if (con != null) con.close();
	}
	
	public static void close(ResultSet rs, PreparedStatement st, Connection con) throws SQLException {
		if (rs != null) rs.close();
		if (st != null) st.close();
		if (con != null) con.close();
	}
}
