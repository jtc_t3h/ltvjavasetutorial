package vn.t3h.ltv243.module2.chapter5;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public abstract class ConnectionPooling <T> {

	Long expireTime = 10000L;
//	List<T> pool = new ArrayList<>();
	
	Map<T, Long> pool = new HashMap(); 
	Map<T, Long> inUse = new HashMap<>();
	
	public abstract T create();
	public abstract void delete(T t);
	public abstract void exipire();
	
	public T checkOut() {
		T t = null;
		if (pool.isEmpty()) {
			t = create();
			inUse.put(t, System.currentTimeMillis());
		} else {
			Long currentTime = System.currentTimeMillis();
			
			Set<T> set = (Set<T>) pool.keySet();
			Iterator<T> iterator = set.iterator();
			while (iterator.hasNext()) {
				T tKey = iterator.next();
				if (currentTime - pool.get(tKey) >= expireTime) {
					pool.remove(tKey);
				}
			}
			
			t = (T) pool.get(pool.entrySet().iterator().next());
		}
		return t;
	}
	
	public void checkIn(T t) {
		pool.put(t, inUse.get(t));
		inUse.remove(t);
	}
}
