package vn.t3h.ltv243.module2.chapter8;

public class CounterThread {

	public static void main(String[] args) {
		Counter counter = new Counter();
		Runnable thread = () -> {
			counter.increment();
//			System.out.println(counter.getCount());
//			try {
//				Thread.sleep(1000);
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
		};
		
		Thread[] t = new Thread[5];
		for (int i = 0; i < 5; i++) {
			t[i] = new Thread(thread);
			t[i].start();
		}
		
		System.out.println("Done.");
	}
}
