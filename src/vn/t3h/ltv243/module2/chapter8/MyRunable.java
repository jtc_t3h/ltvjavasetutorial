package vn.t3h.ltv243.module2.chapter8;

public class MyRunable implements Runnable {

	@Override
	public void run() {

		// thuc hien tac vu --> vd: duyet danh sach 100 so nguyen dau tien
		for (int i = 1; i <= 100; i++) {
			System.out.println("So nguyen: " + i);
		}
	}

	public static void main(String[] args) {
		MyRunable myRunableThread = new MyRunable();
		
		Thread thread = new Thread(myRunableThread);
		thread.start();
		System.out.println("Done.");
	}
}
