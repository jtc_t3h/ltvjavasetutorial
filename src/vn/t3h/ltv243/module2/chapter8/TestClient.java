package vn.t3h.ltv243.module2.chapter8;

import java.util.Scanner;

public class TestClient {

	public static void main(String[] args) throws InterruptedException {

		Scanner sc = new Scanner(System.in);
				
		MyThread thread = new MyThread();
		thread.start();
		thread.setPriority(Thread.MIN_PRIORITY);
//		thread.join();
		
		// Lop long cap vo danh
		Runnable myRunnable = new Runnable() {
			
			@Override
			public void run() {
				System.out.print("Nhap n: ");
				int n = sc.nextInt();
				
				for (int i = 101; i <= n; i++) {
					System.out.println(i);
				}
			}
		};
		Thread t = new Thread(myRunnable);
		t.start();
		
		// Lambda expression
		Runnable myRunnable2 = () -> {
			for (int i = 201; i <= 300; i++) {
				System.out.println(i);
				if (i == 350) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		};
		Thread t2 = new Thread(myRunnable2);
		t2.start();
		t2.setPriority(Thread.MAX_PRIORITY);
		
		System.out.println("myThread priority: " + thread.getPriority());
		System.out.println("Done.");
	}

}
