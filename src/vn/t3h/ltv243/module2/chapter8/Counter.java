package vn.t3h.ltv243.module2.chapter8;

public class Counter {

	public int count = 0;
	
	public synchronized void increment() {
		System.out.println(++count);
	}
	
	public int getCount() {
		return count;
	}
}
