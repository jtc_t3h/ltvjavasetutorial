package vn.t3h.ltv243.module2.chapter7;

public class Singleton {

	private static Singleton instance;
	private int count = 0;
	
	private Singleton() {
	}
	
	public static Singleton getInstance() {
		if (instance == null) {
			instance = new Singleton();
		}
		
		return instance;
	}
	
	public void nextCount() {
		System.out.println(++count);
	}
}
