package vn.t3h.ltv243.module2.chapter7;

public class SingletonTest {

	public static void main(String[] args) {
		Singleton singleton1 = Singleton.getInstance();
		Singleton singleton2 = Singleton.getInstance();
		
		singleton1.nextCount();
		singleton1.nextCount();
		singleton2.nextCount();
	}
}
