package vn.t3h.ltv243.module2.chapter7;

public class ConcreteObserverA implements Observer{
	
	private Subject subject;
	
	public ConcreteObserverA(Subject subject) {
		this.subject = subject;
		this.subject.registerObserver(this);
	}
	
	@Override
	public void update() {
		System.out.println("ConcreteObserverA: update method.");
	}

}
