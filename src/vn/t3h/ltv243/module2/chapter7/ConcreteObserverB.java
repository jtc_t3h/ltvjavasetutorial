package vn.t3h.ltv243.module2.chapter7;

public class ConcreteObserverB implements Observer {

//	private Subject subject;
	
	public ConcreteObserverB(Subject subject) {
//		this.subject = subject;
		subject.registerObserver(this);
	}
	
	@Override
	public void update() {
		System.out.println("ConcreteObserverB: update method.");
	}

}
