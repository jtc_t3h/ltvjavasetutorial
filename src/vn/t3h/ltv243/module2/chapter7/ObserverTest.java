package vn.t3h.ltv243.module2.chapter7;


public class ObserverTest {

	public static void main(String[] args) {
		Subject subject = new Subject();
		new ConcreteObserverA(subject);
		new ConcreteObserverB(subject);
		
		System.out.println("observers size = " + subject.getObservers().size());
		
		subject.setState(10);
	}

}
