package vn.t3h.ltv243.module2.chapter7;

public class DecoratorTest {

	public static void main(String[] args) {
		Component component = new ConcreteComponent();
//		component.operation();
		
		Component component2 = new Decorator(component);
		component2.operation();
		
		LibraryItem bookItem = new BookDecorator("abc",20, "Nguyen Van A");
		bookItem.display();
	}

}
