package vn.t3h.ltv243.module2.chapter7;

public interface Component {

	public void operation();
}
