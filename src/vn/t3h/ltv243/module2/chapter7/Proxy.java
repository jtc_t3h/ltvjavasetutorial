package vn.t3h.ltv243.module2.chapter7;

public class Proxy implements Service {

	private Service service;

	@Override
	public void defaultMethod() {
		if (service == null) {
			service = new RealService();
		}
		
		service.defaultMethod();
	}
	
	
}
