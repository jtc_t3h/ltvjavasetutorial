package vn.t3h.ltv243.module2.chapter7;

public class Book implements LibraryItem {

	private String title;
	private int page;
	
	public Book(String title, int page) {
		this.title = title;
		this.page = page;
	}

	@Override
	public void display() {
		System.out.println("Title: " + title + "\tpage: " + page);
	}
	
	
}
