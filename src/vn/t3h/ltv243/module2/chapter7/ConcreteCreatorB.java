package vn.t3h.ltv243.module2.chapter7;

public class ConcreteCreatorB  implements Creator{

	private Product product;
	
	public ConcreteCreatorB() {
		product = new ConcreteProductB();
	}
	
	@Override
	public void factoryMethod() {
		product.create();
	}

}
