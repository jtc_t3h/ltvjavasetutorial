package vn.t3h.ltv243.module2.chapter7;

public class ConcreteComponent implements Component {

	@Override
	public void operation() {
		System.out.println("ConcreteComponent class: operation method.");
	}

}
