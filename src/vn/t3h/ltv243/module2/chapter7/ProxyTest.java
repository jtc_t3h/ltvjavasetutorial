package vn.t3h.ltv243.module2.chapter7;

public class ProxyTest {

	public static void main(String[] args) {
//		Service service = new RealService();
//		service.defaultMethod();
		
		Service service2 = new Proxy();
		service2.defaultMethod();
	}

}
