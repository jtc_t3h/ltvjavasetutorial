package vn.t3h.ltv243.module2.chapter7;

public class BookDecorator implements LibDecorator {

	private String borrower;
	private LibraryItem libraryItem;
	
	public BookDecorator(String title, int page, String borrower) {
		libraryItem = new Book(title, page);
		this.borrower = borrower;
	}
	
	@Override
	public void display() {
		libraryItem.display();
		
		System.out.println("borrower: " + borrower);
	}

}
