package vn.t3h.ltv243.module2.chapter7;

public class ConcreteCreatorA implements Creator{

	private Product product;
	
	public ConcreteCreatorA() {
		product = new ConcreteProductA();
	}
	
	@Override
	public void factoryMethod() {
		product.create();
	}

}
