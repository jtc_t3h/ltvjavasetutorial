package vn.t3h.ltv243.module2.chapter7;

public class VideoDecorator implements LibDecorator {

	private LibraryItem libraryItem;
	
	public VideoDecorator(String title, int minute) {
		libraryItem = new Video(title, minute);
	}
	
	@Override
	public void display() {
		libraryItem.display();
		
		// do something
	}
}
