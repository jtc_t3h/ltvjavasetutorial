package vn.t3h.ltv243.module2.chapter7;

public class Decorator implements Component {

	private Component component;
	
	public Decorator(Component component) {
		this.component = component;
	}

	@Override
	public void operation() {
		component.operation();
		
		// them chuc nang (trach nhiem)
		System.out.println("Decorator class: operation method.");
	}
	
}
