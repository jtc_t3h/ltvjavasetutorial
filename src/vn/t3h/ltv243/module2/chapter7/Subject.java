package vn.t3h.ltv243.module2.chapter7;

import java.util.ArrayList;
import java.util.List;

public class Subject {

	private List<Observer> observers = new ArrayList<>();
	private int state;
	
	public void registerObserver(Observer observer) {
		observers.add(observer);
	}
	
	public void unRegisterObserver(Observer observer) {
		observers.remove(observer);
	}
	
	public void notifyObserver() {
		for (Observer observer: observers) {
			observer.update();
		}
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
		
		notifyObserver();
	}

	public List<Observer> getObservers() {
		return observers;
	}

	public void setObservers(List<Observer> observers) {
		this.observers = observers;
	}
	
	
	
}
