package vn.t3h.ltv243.module2.chapter7;

public class FactoryTest {

	public static void main(String[] args) {

		Creator creator = new ConcreteCreatorA();
		creator.factoryMethod();
	}

}
