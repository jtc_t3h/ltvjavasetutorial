package vn.t3h.ltv243.module2.chapter7;

public class Video implements LibraryItem {

	private String title;
	private int minute;
	
	public Video(String title, int minute) {
		this.title = title;
		this.minute = minute;
	}

	@Override
	public void display() {
		System.out.println("Title: " + title + "\tminute: " + minute);
	}
	
	
}
