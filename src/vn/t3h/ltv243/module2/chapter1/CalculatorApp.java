package vn.t3h.ltv243.module2.chapter1;

public class CalculatorApp {

	public static void main(String[] args) {
		
		/* Truoc Java 8: Anonymous inner class
		 * Khai bao bien
		 */
		Calculator calculator = new Calculator() {
			
			@Override
			public int add(int a, int b) {
				return a + b;
			}
		};
		
		// Goi ham
		System.out.println(calculator.add(5, 6));
		
		/*
		 * Java 8: dung bieu thuc Lambda
		 */
		Calculator cal2 = (int a, int b) -> {
			System.out.println("Ham tinh tong 2 so");
			return a + b;
		};
		System.out.println(cal2.add(5, 6));
	}

}
