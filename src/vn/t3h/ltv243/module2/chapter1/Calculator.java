package vn.t3h.ltv243.module2.chapter1;

/**
 * Dinh nghia mot Function Interface = Single Abstract Method
 * @author MoonLuna
 *
 */
public interface Calculator {

	public int add(int a, int b);
	
	// Function Interface chi co 1 ham duy nhat, nhung co the hien
	// bo sung them 2 dang function: static method va default method
	
	public static void staticMethod() {
		System.out.println("Static Method");
	}
	
	public default void defaultMethod() {
		System.out.println("Default Method");
	}
}
