package vn.t3h.ltv243.module2.chapter1;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class ListApp {

	public static void main(String[] args) {

		List<Integer> list1 = Arrays.asList(1,2,3,4,5,6,7,8,9);
		
		// Bai toan: duyet cac phan tu cua list
		// Cach 1: truoc Java 8 -> cau truc for-each
//		for (int element: list1) {
//			System.out.println(element);
//		}
		
		// Cach 2: su dung lambda expression
//		Consumer<Integer> consumer = element -> System.out.println(element);
//		list1.forEach(consumer);
//		
//		list1.forEach(element -> System.out.println(element));
		
		list1.sort((element1, element2) -> element2.compareTo(element1));
		list1.forEach(element -> System.out.print(element + ","));
		
		
	}

}
