package vn.t3h.ltv243.module2.chapter2;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class CreateStream {

	public static void main(String[] args) {

		Stream<Integer> stream1 = Stream.of(1,6,2,4,9,0);
		
		List<String> list = Arrays.asList("Hello", "world", "stream", "Java8");
		Stream<String> stream2 = list.stream();
		
		Stream<Double> stream3 = Stream.generate(() -> Math.random());
		
		Stream<Character> stream4 = Stream.iterate('a', (i) -> i++);
		
		Stream<Float> stream5 = Arrays.stream(new Float[] {1.1F, 2.2F, 3.3F});
		
		
		////////////////////////////////////////////////////////////////////
//		System.out.println("Stream: forEach function");
//		stream1.forEach(e -> System.out.println(e));
		
//		System.out.println("Stream: map function");
//		stream2.map(e -> e + e).forEach(System.out::println);
//		
//		System.out.println("Stream: filter function");
//		stream2.filter(e -> e.contains("e")).forEach(System.out::println);
		
		stream2.map(e -> e + e).peek(e -> System.out.println("peek+" + e)).filter(e -> e.contains("e")).forEach(System.out::println);
	
		System.out.println("Stream: sort function");
//		stream1.sorted().forEach(System.out::println);
		stream1.sorted((e1, e2) -> e2 - e1).forEach(System.out::println);
	}

}
