package vn.t3h.ltv243.module2.chapter3;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class WriteJsonBySimple {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		
		JSONArray peopleArr = new JSONArray();
		
		JSONObject people1 = new JSONObject();
		people1.put("id", 1);
		people1.put("name", "A");
		
		JSONObject people2 = new JSONObject();
		people2.put("id", 2);
		people2.put("name", "B");
		
		peopleArr.add(people1);
		peopleArr.add(people2);
		
		try (FileWriter fw = new FileWriter(new File("src/vn/t3h/ltv243/module2/chapter3/ds_people.json"))){
			fw.write(peopleArr.toJSONString());
			fw.flush();
			
			System.out.println("Done!");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
}
