package vn.t3h.ltv243.module2.chapter3;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class ReadJSonBySimple {

	public static void main(String[] args) throws ParseException, FileNotFoundException, IOException {

		JSONParser parser = new JSONParser();
		JSONObject personObj =  (JSONObject) parser.parse(new FileReader("src/vn/t3h/ltv243/module2/chapter3/thongtincanhan.json"));
		System.out.println(personObj);
		
		System.out.println(personObj.get("name"));
		System.out.println(personObj.get("address"));
		System.out.println(personObj.get("age"));
		
		JSONArray coursesArray = (JSONArray) personObj.get("courses");
		System.out.println(coursesArray);
		for (int idx = 0; idx < coursesArray.size(); idx++) {
			System.out.println(coursesArray.get(idx));
		}
		
		JSONObject classObj = (JSONObject) personObj.get("myclass");
		System.out.println(classObj.get("id"));
		System.out.println(classObj.get("name"));
	}

}
