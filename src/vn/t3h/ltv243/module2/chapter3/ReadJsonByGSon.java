package vn.t3h.ltv243.module2.chapter3;

import java.io.FileNotFoundException;
import java.io.FileReader;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

public class ReadJsonByGSon {

	public static void main(String[] args) throws JsonIOException, JsonSyntaxException, FileNotFoundException {

		Gson gson = new Gson();
		People people = gson.fromJson(new FileReader("src/vn/t3h/ltv243/module2/chapter3/thongtincanhan.json"), People.class);
		
		System.out.println(people.getName());
		System.out.println(people.getAddress());
		
		MyClass myClass = people.getMyclass();
		System.out.println(myClass.getId());
		System.out.println(myClass.getName());
	}

}
