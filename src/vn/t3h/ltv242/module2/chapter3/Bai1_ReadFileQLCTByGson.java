package vn.t3h.ltv242.module2.chapter3;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

public class Bai1_ReadFileQLCTByGson {

	public static void main(String[] args) throws UnsupportedEncodingException, FileNotFoundException {
		Gson gson = new Gson();
		JsonReader fileReader = new JsonReader(new InputStreamReader(new FileInputStream("src/vn/t3h/ltv242/module2/chapter3/QLCT_1.json"), "UTF-8"));
		
		Bai1_CongTyAndDonVi congTyAndDonVi = gson.fromJson(fileReader, Bai1_CongTyAndDonVi.class);
		
		System.out.println("Tên công ty: " + congTyAndDonVi.getCONG_TY()[0].getTen());
	}

}
