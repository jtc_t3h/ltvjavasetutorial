package vn.t3h.ltv242.module2.chapter3;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class ReadFileBySimpleJSon {

	public static void main(String[] args) throws FileNotFoundException, IOException {

		// Buoc 1: Tao doi tuong JSONParse
		JSONParser parser = new JSONParser();
		
		// Buoc 2: parse noi dung cu file -> tra ve Object
		try (FileReader file = new FileReader("src/vn/t3h/ltv242/module2/chapter3/books.json")){
			Object obj = parser.parse(file);
			
			JSONObject jsonObj = (JSONObject) obj;
						
			JSONArray arrBooks = (JSONArray) jsonObj.get("books");
			
			Iterator iterator = arrBooks.iterator();
			List<Book> list = new ArrayList<>();
			while(iterator.hasNext()) {
				JSONObject bookObj = (JSONObject) iterator.next();
				System.out.println(bookObj.get("language") + " == " + bookObj.get("edition"));
				
				Book book = new Book(bookObj.get("language").toString(), bookObj.get("edition").toString());
				list.add(book);
			}
			
			// Lay ra list book
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
	}

}
