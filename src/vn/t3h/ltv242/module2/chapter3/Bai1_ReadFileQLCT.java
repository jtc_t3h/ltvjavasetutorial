package vn.t3h.ltv242.module2.chapter3;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Bai1_ReadFileQLCT {

	public static void main(String[] args) throws FileNotFoundException, IOException, ParseException {

		StringBuilder sb = new StringBuilder("******* Thông tin công ty ***********\n");
		
		JSONParser parser = new JSONParser();
		try (InputStreamReader file = new InputStreamReader(new FileInputStream("src/vn/t3h/ltv242/module2/chapter3/QLCT_1.json"), "UTF-8")){
			JSONObject obj = (JSONObject) parser.parse(file);
			
			// thong tin cong ty
			JSONArray congTyArr =  (JSONArray) obj.get("CONG_TY");
			Iterator iCongTyArr = congTyArr.iterator();
			while (iCongTyArr.hasNext()) {
				JSONObject congTy = (JSONObject) iCongTyArr.next();
				
				sb.append("Ten cong ty: " + congTy.get("Ten") + "\n");
				sb.append("Địa chỉ: " + congTy.get("Dia_chi") + "\n");
			}
			
			// thong tin don vi
			JSONArray donViArr =  (JSONArray) obj.get("DON_VI");
			sb.append("Tổng số đơn vị: " + donViArr.size() + "\n");
			
			Iterator iDonViArr = donViArr.iterator();
			while (iDonViArr.hasNext()) {
				JSONObject donVi = (JSONObject) iDonViArr.next();
				
			}
			
			System.out.println(sb.toString());
		}
	}

}
