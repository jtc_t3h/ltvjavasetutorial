package vn.t3h.ltv242.module2.chapter3;

import java.io.FileNotFoundException;
import java.io.FileReader;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

public class ReadFileByGSon {

	public static void main(String[] args) throws FileNotFoundException {
		Gson gson = new Gson();
		JsonReader fileReader = new JsonReader(new FileReader("src/vn/t3h/ltv242/module2/chapter3/books.json"));
		
		ArrayBook arrBook = gson.fromJson(fileReader, ArrayBook.class);
		
		// duyet danh sach book
		for (Book book: arrBook.getBooks()) {
			System.out.println(book.getLanguage() + " == " + book.getEdition());
		}
	}

}
