package vn.t3h.ltv242.module2.chapter3;

public class Book {

	private String language;
	private String edition;
	
	

	public Book(String language, String edition) {
		this.language = language;
		this.edition = edition;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getEdition() {
		return edition;
	}

	public void setEdition(String edition) {
		this.edition = edition;
	}

}
