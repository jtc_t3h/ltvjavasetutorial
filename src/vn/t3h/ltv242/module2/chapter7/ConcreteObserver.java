package vn.t3h.ltv242.module2.chapter7;

public class ConcreteObserver implements Observer {

	private int state = 0;
	private Subject subject;
	
	public ConcreteObserver(Subject subject) {
		this.subject = subject;
		this.subject.attach(this);		
	}
	
	@Override
	public void update(int state) {
		this.state = state;
		System.out.println("ConcreteObserver: update method.");
	}

	public int getState() {
		return state;
	}
}
