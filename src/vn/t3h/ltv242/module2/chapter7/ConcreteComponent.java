package vn.t3h.ltv242.module2.chapter7;

public class ConcreteComponent implements Component{

	@Override
	public void operator() {
		System.out.println("ConcreteComponent: operator method.");
	}

}
