package vn.t3h.ltv242.module2.chapter7;

public class Creator {
	
	public Product getProduct(int type) {
		Product product = null;
		if (type == 1) {
			product = new ConcreteProductA();
		} else {
			product = new ConcreteProductB();
		}
		return product;
	}
}
