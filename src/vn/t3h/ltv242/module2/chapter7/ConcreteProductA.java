package vn.t3h.ltv242.module2.chapter7;

public class ConcreteProductA extends Product {

	@Override
	public void createProduct() {
		System.out.println("ConcreteProductA: createProduct method.");
	}

}
