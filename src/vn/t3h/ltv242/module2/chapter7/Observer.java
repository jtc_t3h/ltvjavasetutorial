package vn.t3h.ltv242.module2.chapter7;

public interface Observer {

	public void update(int state);
	public int getState();
}
