package vn.t3h.ltv242.module2.chapter7;

public class ObserverApp {

	public static void main(String[] args) {
		Subject subject = new Subject();
		
		Observer observer1 = new ConcreteObserver(subject);
		Observer observer2 = new ConcreteObserver(subject);
		System.out.println(observer1.getState());
		System.out.println(observer2.getState());
		
		subject.setState(10);
		
		System.out.println(observer1.getState());
		System.out.println(observer2.getState());

	}

}
