package vn.t3h.ltv242.module2.chapter7;

public class DecoratorApp {

	public static void main(String[] args) {
		DecoratorComponent component = new DecoratorComponent();
		component.operator();
		component.aMethod();
	}

}
