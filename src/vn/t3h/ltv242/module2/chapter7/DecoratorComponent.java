package vn.t3h.ltv242.module2.chapter7;

public class DecoratorComponent implements Component{

	private Component component;
	
	public DecoratorComponent() {
		component = new ConcreteComponent();
	}
	
	@Override
	public void operator() {
		component.operator();
	}

	public void aMethod() {
		System.out.println("DecoratorComponent: aMethod method.");
	}
}
