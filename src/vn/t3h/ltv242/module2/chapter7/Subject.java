package vn.t3h.ltv242.module2.chapter7;

import java.util.ArrayList;
import java.util.List;

public class Subject {

	private int state = 0;
	private List<Observer> observers= new ArrayList<>();
	
	public void attach(Observer observer) {
		this.observers.add(observer);
	}
	
	public void notifyObserver() {
		for (Observer observer: observers) {
			observer.update(state);
		}
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
		notifyObserver();
	}
	
	
}
