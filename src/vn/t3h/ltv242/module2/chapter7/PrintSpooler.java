package vn.t3h.ltv242.module2.chapter7;

public class PrintSpooler {

	private static PrintSpooler instance;
	
	private PrintSpooler() {}
	
	public static PrintSpooler getInstance() {
		if (instance == null) {
			instance = new PrintSpooler();
		}
		
		return instance;
	}
	
	public void print() {
		System.out.println("PrintSpooler: print method.");
	}
	
	public void endPrint() {
		System.out.println("PrintSpooler: endPrint method.");
	}
	
}
