package vn.t3h.ltv242.module2.chapter7;

public class PrintSpoolerApp {

	public static void main(String[] args) {

		PrintSpooler printSpooler = PrintSpooler.getInstance();
		PrintSpooler printSpooler2 = PrintSpooler.getInstance();
		
		printSpooler.print();
		printSpooler2.print();
	}

}
