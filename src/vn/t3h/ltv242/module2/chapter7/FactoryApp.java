package vn.t3h.ltv242.module2.chapter7;

import java.util.Scanner;

public class FactoryApp {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.print("Nhap product can tao ra (1: sp A, nguoc lai sp B): ");
		int type = sc.nextInt();
		Creator creator = new Creator();
		Product product = creator.getProduct(type);
		product.createProduct();
	}

}
