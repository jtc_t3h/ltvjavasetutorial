package vn.t3h.ltv242.module2.chapter7;

public class ConcreteProductB extends Product{

	@Override
	public void createProduct() {
		System.out.println("ConcreteProductB: createProduct method.");
	}

}
