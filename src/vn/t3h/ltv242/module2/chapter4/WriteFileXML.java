package vn.t3h.ltv242.module2.chapter4;

import java.io.File;
import java.io.FileOutputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class WriteFileXML {

	public static void main(String[] args) throws ParserConfigurationException, TransformerException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.newDocument();
		
		Element root = document.getDocumentElement();
		root = document.createElement("sinhviens");
		document.appendChild(root);
		
		// the <sinhvien>
		Element sinhVienNode = document.createElement("sinhvien");
		
		Element idNode = document.createElement("id");
		idNode.setTextContent("1");
		sinhVienNode.appendChild(idNode);
		
		Element nameNode = document.createElement("name");
		nameNode.setTextContent("A");
		sinhVienNode.appendChild(nameNode);
		
		root.appendChild(sinhVienNode);
		
		// chuyen DOM -> XMLfile
		TransformerFactory tfFactory = TransformerFactory.newInstance();
		Transformer transformer = tfFactory.newTransformer();
		DOMSource source = new DOMSource(document);
		
		StreamResult result = new StreamResult(new File("src/vn/t3h/ltv242/module2/chapter4/sinhviens.xml"));
		transformer.transform(source, result);
		System.out.println("Done!");
	}

}
