package vn.t3h.ltv242.module2.chapter4;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ReadFileXML {

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.parse(new File("src/vn/t3h/ltv242/module2/chapter4/books.xml"));
		
//		System.out.println(document.getNodeName());
		
		NodeList bookStoreNodeList = document.getElementsByTagName("bookstore");
		Node bookStore = bookStoreNodeList.item(0);
		System.out.println(bookStore.getNodeName());
//		System.out.println(bookStore.getNodeValue());
		
		NodeList bookStoreChildNodeList = bookStore.getChildNodes();
		System.out.println("bookStoreChildNodeList = " + bookStoreChildNodeList.getLength());
		// duyet danh sach cac node
		for (int idx = 0; idx < bookStoreChildNodeList.getLength(); idx++) {
			Node bookStoreChildNode = bookStoreChildNodeList.item(idx);
			if (bookStoreChildNode.getNodeName().equals("book")) {
				
				NodeList bookChildNodeList = bookStoreChildNode.getChildNodes();
				System.out.println(bookChildNodeList.getLength());
				// duyet cac node con cua <book>
				for (int i = 0; i < bookChildNodeList.getLength(); i++ ) {
					Node node = bookChildNodeList.item(i);
					
					
					if (node.getNodeName().equals("title")) {
						System.out.println(node.getTextContent());
						
						
						NamedNodeMap attributs = node.getAttributes();
						
						System.out.println(attributs.getNamedItem("lang").getNodeName());
						System.out.println(attributs.getNamedItem("lang").getNodeValue());
					}
				}
			}
		}
	}

}
