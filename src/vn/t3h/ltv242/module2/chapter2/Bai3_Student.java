package vn.t3h.ltv242.module2.chapter2;

public class Bai3_Student {

	private String name;
	private int age;
	private double mark1;
	private double mark2;
	
	// diem TB (avg)
	private double avg;

	public Bai3_Student(String name, int age, double mark1, double mark2) {
		this.name = name;
		this.age = age;
		this.mark1 = mark1;
		this.mark2 = mark2;
		
		this.avg = (this.mark1 + this.mark2)/2;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public double getMark1() {
		return mark1;
	}

	public void setMark1(double mark1) {
		this.mark1 = mark1;
	}

	public double getMark2() {
		return mark2;
	}

	public void setMark2(double mark2) {
		this.mark2 = mark2;
	}

	public double getAvg() {
		return avg;
	}

	public void setAvg(double avg) {
		this.avg = avg;
	}
	
	@Override
	public String toString() {
		return name + "\t\tage = " + age + "\t\tmark1 = " + mark1 + "\t\tmark2 = " + mark2 + "\t\tavg = " + avg;
	}
}
