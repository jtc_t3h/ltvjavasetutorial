package vn.t3h.ltv242.module2.chapter2;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Bai3_Stream {

	public static void main(String[] args) {
		List<Bai3_Student> list = new ArrayList<>();
		list.add(new Bai3_Student("Tran Giao Bao", 19, 7.0, 8.0));
		list.add(new Bai3_Student("Nguyen Phan Minh Tri", 21, 6.0, 7.0));
		list.add(new Bai3_Student("Bui Nguyen Minh Hieu", 23, 6.0, 7.0));
		list.add(new Bai3_Student("Phung Thanh Son", 25, 1.0, 1.0));
		list.add(new Bai3_Student("Nguyen Tran Minh Nhut", 19, 10.0, 10.0));
		list.add(new Bai3_Student("Nguyen Chi Cuong", 19, 9.0, 9.0));
		list.add(new Bai3_Student("Le Minh Khanh", 19, 9.0, 9.0));
		list.add(new Bai3_Student("Nguyen Vo Thuy Vy", 20, 5.0,5.0));
		list.add(new Bai3_Student("Nguyen Hong Thang", 19, 6.0,6.0));
		list.add(new Bai3_Student("Nguyen Thai Tan Thanh", 20, 2.0, 3.0));
		
		System.out.println("List of student:");
		list.stream().forEach(student -> System.out.println(student));
		
		long numberOfStudentFrom18 = list.stream().filter(student -> student.getAge() >= 20).count();
		System.out.println("Number of student have old >= 18: " + numberOfStudentFrom18);
		
		System.out.println("Numbers of student have name with start N character:");
		System.out.println(list.stream().filter(student -> student.getName().startsWith("N")).peek(student -> System.out.println(student)).count());
	
		System.out.println("\nStuden have max score:");
	    System.out.println(list.stream().max( (student1, student2) -> (int) (student1.getAvg() - student2.getAvg())).get());
	}
}
