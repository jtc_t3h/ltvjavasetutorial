package vn.t3h.ltv242.module2.chapter2;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamAPIDemo {

	public static void main(String[] args) {

		// Tao doi tuong Stream
		// Cach 1: su dung phuong of cua Stream
		Stream<Integer> stream1 = Stream.of(1,2,3,4,5,6,7,8,9,10);
		Stream<Integer> stream2 = Stream.of(new Integer[]{11,12,13,14});
		
		// Cach 2: Su dung Collection Framework
		List<String> list = new ArrayList<String>();
		list.add("Hello");
		list.add("World");
		list.add("Stream");
		list.add("API");
		
		Stream<String> stream3 = list.stream();
		Stream<String> stream4 = list.parallelStream(); 
		
		// Cach 3: su dung phuong thuc Generate cua lop Stream
		Stream<Double[]> stream5 = Stream.generate(() -> new Double[]{1.1,2.2,3.3});
		
		Random rd = new Random();
		IntStream stream6 = rd.ints(0, 10).limit(10);
		stream6.forEach(e -> System.out.println(e));
        
		System.out.println("binh phuong");
		System.out.println(stream1.map((e) -> e*e).peek(e -> System.out.println(e)).filter(e -> e == 36).count());
		
		
		stream2.sorted((e1, e2) -> e2 - e1).forEach(e -> System.out.println(e));
	}

}
