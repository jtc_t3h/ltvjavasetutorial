package vn.t3h.ltv242.module2.chapter5;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class TestClient {

	public static void main(String[] args) throws SQLException {

		String mySQLDriverClass = "com.mysql.cj.jdbc.Driver";
		
		try {
			Class.forName(mySQLDriverClass);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			System.out.println("Driver lib missing.");
		}
		
		String url = "jdbc:mysql://localhost:3306/phan_cong_nhan_vien_1_1_nam";
		String username = "root";
		String password = "";
		
		Connection con = null;
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			con = DriverManager.getConnection(url, username, password);
			
			DatabaseMetaData meta = con.getMetaData();
			System.out.println(meta.getDatabaseProductName());
			System.out.println(meta.getDatabaseProductVersion());
			
			statement = con.createStatement();
			String sqlInsert = "insert into don_vi values(7, 'Don Vi G')";
			String sqlSelect = "select * from don_vi_gc";
			
//			statement.executeUpdate(sqlInsert);
			resultSet = statement.executeQuery(sqlSelect);
			
			while (resultSet.next()) {
				System.out.println("ID = " + resultSet.getInt(1) + ", Name =" + resultSet.getString("Ten"));
			}
			
//			resultSet.close();
//			statement.close();
//			con.close();
		} catch (SQLException e) {
			System.out.println("Connect unsuccessful.");
			e.printStackTrace();
		} finally {
			resultSet.close();
			statement.close();
			con.close();
		}
		
	}

}
