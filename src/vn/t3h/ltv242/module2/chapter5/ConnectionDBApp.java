package vn.t3h.ltv242.module2.chapter5;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectionDBApp {

	public static void main(String[] args) {
		String mySQLDriverClass = "com.mysql.cj.jdbc.Driver";
		String url = "jdbc:mysql://localhost:3306/phan_cong_nhan_vien_1_1_nam";
		String username = "root";
		String password = "";
		String sqlSelect = "select * from don_vi";
		
		try(
			Connection con = DriverManager.getConnection(url, username, password);
			Statement statement = con.createStatement();
			ResultSet rs = statement.executeQuery(sqlSelect)){
			
			while (rs.next()) {
				System.out.println("ID = " + rs.getInt(1) + ", Name =" + rs.getString("Ten"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
