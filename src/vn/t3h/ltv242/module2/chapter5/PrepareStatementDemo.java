package vn.t3h.ltv242.module2.chapter5;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class PrepareStatementDemo {

	public static void main(String[] args) throws SQLException {
		String url = "jdbc:mysql://localhost:3306/phan_cong_nhan_vien_1_1_nam";
		String username = "root";
		String password = "";
		String sql = "insert into don_vi values (?,?)";

		try (Connection con = DriverManager.getConnection(url, username, password);
				PreparedStatement ps = con.prepareStatement(sql)) {
			con.setAutoCommit(false);

			try {
				ps.setInt(1, 11);
				ps.setString(2, "Don Vi K");

				ps.executeUpdate();

				ps.setInt(1, 10);
				ps.setString(2, "Don Vi L");
				ps.executeUpdate();
			} catch (SQLException e) {
				con.rollback();
				e.printStackTrace();
			}

			con.commit();
		}

	}

}
