package vn.t3h.ltv242.module2.chapter8;

public class Counter {

	private int count;
	
	public void increment() {
		synchronized (this) {
			count += 1;
		}
		
	}
	
	public synchronized int getCount() {
		return ++count;
	}
}
