package vn.t3h.ltv242.module2.chapter8;

public class SynronizedTest {

	public static void main(String[] args) throws InterruptedException {
		Counter counter = new Counter();

		Runnable myRunable = () -> {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println(counter.getCount());
		};
		
		Thread arrThread[] = new Thread[5];
		for (int idx = 0; idx < 5; idx++) {
			arrThread[idx] = new Thread(myRunable);
			arrThread[idx].setPriority(10 - idx);
			arrThread[idx].start();
		}
		
////		Thread.sleep(5000);
//		System.out.println(counter.getCount());
	}

}
