package vn.t3h.ltv242.module2.chapter8;

public class ThreadClient {

	public static void main(String[] args) {
		MyThread[] mt = new MyThread[5];
		for (int idx = 0; idx < mt.length; idx++) {
			mt[idx] = new MyThread();
			mt[idx].start();
		}
		
		MyRunable mr = new MyRunable();
		Thread thread = new Thread(mr);
		thread.start();		
		
		System.out.println("Done.");
		System.out.println("or not done.");
		System.out.println(thread.getName() + "==================");
	}
}
