package vn.t3h.ltv242.module2.chapter8;

public class MyRunable implements Runnable{

	@Override
	public void run() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		for (int i = 0; i < 1000; i++) {
			System.out.println(i);
		}
	}

}
