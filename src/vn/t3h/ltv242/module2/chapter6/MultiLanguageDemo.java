package vn.t3h.ltv242.module2.chapter6;

import java.util.Locale;
import java.util.ResourceBundle;

public class MultiLanguageDemo {

	public static void main(String[] args) {
		Locale locale = new Locale("en", "US");
	
		ResourceBundle bundle = ResourceBundle.getBundle("vn.t3h.ltv242.module2.chapter6.StatsBundle", locale);
		System.out.println(bundle.getString("username"));
		System.out.println(bundle.getString("password"));
	}

}
