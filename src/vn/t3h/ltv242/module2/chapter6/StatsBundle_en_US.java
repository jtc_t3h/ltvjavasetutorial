package vn.t3h.ltv242.module2.chapter6;

import java.util.ListResourceBundle;

public class StatsBundle_en_US extends ListResourceBundle{

	@Override
	protected Object[][] getContents() {
		return new Object[][] {
			{"username", "User Name"},
			{"password", "Password"}
		};
	}

}
