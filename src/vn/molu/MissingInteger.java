package vn.molu;

public class MissingInteger {

	public static void main(String[] args) {
		MissingInteger solution = new MissingInteger();
		
		int A1[] = {1, 3, 6, 4, 1, 2};
		int A2[] = {1, 2, 3};
		int A3[] = {-1, -3};
		System.out.println(solution.solution(A1));
		System.out.println(solution.solution(A2));
		System.out.println(solution.solution(A3));
	}

	public int solution(int[] A) {
		if (A.length == 0) {
			return 1;
		}
		
		int max = A[0];
		for (int idx = 1; idx < A.length; idx++) {
			if (max < A[idx]) {
				max = A[idx];
			}
		}
		if (max < 0) {
			return 1;
		}
		
		int smallest = max + 1;
		for (int i = 1; i < max; i++) {
			if (!isContain(A, i)) {
				return i;
			}  
		}
		return smallest;
	}

	private boolean isContain(int[] a, int i) {
		for (int e: a) {
			if (e == i) {
				return true;
			}
		}
		return false;
	}
}
