package vn.molu;

public class BulbsTurnOn {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		BulbsTurnOn b = new BulbsTurnOn();
		
		int [] A1 = {2, 1, 3, 5, 4};
		int [] A2 = {1,3,4,2,5};

		System.out.println(b.solution(A1));
		System.out.println(b.solution(A2));
	}
	
	public int solution(int[] A) {		
		int turnOn = 0;
		
		String s = "";
        for (int element: A) {
        	if (element == 1 || isPreviousOn(s, element)) {
        		turnOn++;
        	}
        	s += String.valueOf(element);
        }
        
		return turnOn;	
    }

	private boolean isPreviousOn(String s, int element) {
		for (int i = 1; i < element; i++) {
			if (!s.contains(String.valueOf(i))) {
				return false;
			}
		}
		return true;
	}

}
