package vn.molu;

import java.util.Arrays;

public class MissingInteger2 {

	public static void main(String[] args) {
		MissingInteger2 solution = new MissingInteger2();
		
		int A1[] = {1, 3, 6, 4, 1, 2};
		int A2[] = {1, 2, 3};
		int A3[] = {-1, -3};
		System.out.println(solution.solution(A1));
		System.out.println(solution.solution(A2));
		System.out.println(solution.solution(A3));
	}

	public int solution(int[] arr) {
		int smallest = 1;
		
		Arrays.sort(arr);		
		for (int e: arr) {
			if (e == smallest) {
				smallest = smallest + 1;
			}
		}
		
		return smallest;
	}
}
