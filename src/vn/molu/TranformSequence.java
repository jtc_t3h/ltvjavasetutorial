package vn.molu;

public class TranformSequence {

	public static void main(String[] args) {
		TranformSequence c = new TranformSequence();
		
		System.out.println(c.solution("ACCAAABBC"));
		System.out.println(c.solution("ABCBBCBA"));
		System.out.println(c.solution("BABABA"));
	}

	public String solution(String S) {
        if (S.contains("AA")) {
        	S = S.replaceFirst("AA", "");
        } else if (S.contains("BB")) {
        	S = S.replaceFirst("BB", "");
        } else if (S.contains("CC")) {
        	S = S.replaceFirst("CC", "");
        } else {
        	return S;
        }
		return solution(S);
    }
}
